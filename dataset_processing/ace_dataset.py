import glob
import json
import os.path
import pickle
import random
import sys
from random import shuffle

import numpy as np
import torch
from torch.utils.data import Dataset

from config.config import BaseConfig
from dataset_processing.data_utils import pad_label_sequences, pad_scatter_sequences

from models.cnn.tokenizer import Tokenizer
from models.cnn.embedder import TransformersEncoder

class ACE_DATASET(Dataset):

    multiple_label = None
    
    def __init__(self, config: BaseConfig, typedt="dev", path="all", partial=None, min_len=0, max_len=40, rebuild=False):
        self.config = config
        reload_status = False
        self.ori_token_sents = []
        self.entity_ids_sents = []
        self.input_char_ids_sents = []
        self.input_ids_sents = []
        self.input_mask_sents = []
        self.input_word_mask_sents = []
        self.input_segment_sents = []
        self.scatter_offset_sents = []
        self.edge_id_sents = []
        self.trigger_ids_sents = []
        self.anchor_ids_sents = []
        self.arg_id_sents = []
        self.node_node_adj_sents = []
        self.node_edge_map_sents = []
        self.edge_node_map_sents = []
        self.edge_edge_adj_sents = []
        self.len_sents = []
        # dev: 30, test: 40, train: 529
        if path == "all":
            files = glob.glob(config.reload_path + "*_{}.pkl".format(typedt))
            if len(files) > 0:
                pass
        else:
            if not rebuild and os.path.exists(config.reload_path + path):
                reload_status = True
                reload_dataset = pickle.load(open(config.reload_path + path, 'rb'))
                dict_attrs = reload_dataset.__dict__
                for attr in dict_attrs:
                    self.__setattr__(attr, dict_attrs[attr])

        if not reload_status:
            tokenizer = Tokenizer("./bert-base-cased", return_sparse_offsets=False)
            data = json.load(open(path, 'r', encoding="utf8"))
            countf = 0
            print("TOTAL FILES: ", len(data))

            for idf, file in enumerate(data):
                doc = data[file]
                for sent in doc:
                    if len(sent['words']) < 2:
                        continue

                    if (max_len is not None and len(sent['words']) >= max_len) or (min_len is not None and len(sent['words']) < min_len):
                        continue

                    tokens = tokenizer([sent['words']], is_split_into_words=True).data
                    ori_token_sent = sent['words']
                    entity_ids_sent = [[config.entity_vocab.get(etype) for etype in word] for word in sent['entities']]
                    input_char_ids_sent = [[config.char_vocab.get(char, config.char_vocab.get(config.UNK)) for char in word] for word in sent['words']]
                    input_ids_sent = tokens['input_ids'][0]
                    input_mask_sent = tokens['attention_mask'][0]
                    input_segment_sent = tokens['token_type_ids'][0]
                    scatter_offset_sent = tokens['scatter_offsets'][0]
                    edge_id_sent = sent.get('edge_ids', [])

                    node_node_adj = sent.get('adj', {"indices": [], "values": []})
                    node_edge_map = sent.get('node_edge_adj_v2', {"indices": [], "values": []})
                    edge_node_map = sent.get('node_map_v2', {"indices": [], "values": []})
                    edge_edge_adj = sent.get('edge_adj', {"indices": [], "values": []})

                    trigger_ids_sent = [config.trigger_vocab.get(config.O_tag) for _ in sent['words']]
                    anchor_ids_sent = [0 for _ in sent['words']]
                    for event in sent['golden-event-mentions']:
                        if event['trigger']['start'] >= 0:
                            if event['trigger']['end'] - event['trigger']['start'] <= 0:
                                event['trigger']['end'] = event['trigger']['start'] + len(event['trigger']['text'].split())

                            prefix = "B-"
                            for tok_id in range(event['trigger']['start'], event['trigger']['end']):
                                if tok_id < len(trigger_ids_sent):
                                    trigger_ids_sent[tok_id] = config.trigger_vocab.get(prefix + event['event_type'])
                                    if prefix == "B-":
                                        prefix = "I-"
                                        anchor_ids_sent[tok_id] = 1

                    arg_id_sent = {"indices": sent['arg_map']['indices'],
                                   "values": [config.argument_vocab[label] if "time" not in label.lower() else config.argument_vocab.get(label[:2] + "Time")
                                              for label in sent['arg_map']['values']]}
                    pairs = []
                    for row, col in zip(*sent['arg_map']['indices']):
                        if "{}_{}".format(row, col) not in pairs:
                            pairs.append("{}_{}".format(row, col))
                        else:
                            print('x')
                    self.ori_token_sents.append(ori_token_sent)
                    self.entity_ids_sents.append(entity_ids_sent)
                    self.input_char_ids_sents.append(input_char_ids_sent)
                    self.input_ids_sents.append(input_ids_sent)
                    self.input_mask_sents.append(input_mask_sent)
                    self.input_segment_sents.append(input_segment_sent)
                    self.scatter_offset_sents.append(scatter_offset_sent)
                    self.edge_id_sents.append(edge_id_sent)
                    self.node_node_adj_sents.append(node_node_adj)
                    self.node_edge_map_sents.append(node_edge_map)
                    self.edge_node_map_sents.append(edge_node_map)
                    self.edge_edge_adj_sents.append(edge_edge_adj)
                    self.trigger_ids_sents.append(trigger_ids_sent)
                    self.anchor_ids_sents.append(anchor_ids_sent)
                    self.arg_id_sents.append(arg_id_sent)
                    self.len_sents.append(len(sent['words']))
                    self.input_word_mask_sents.append([1 for _ in range(len(sent['words']))])

                    pass

                if partial is not None and idf >= partial and idf % partial == 0:
                    pickle.dump(self, open(config.reload_path + "sub_dataset_{}_{}_{}_{}.pkl".format(min_len, max_len, countf, typedt), 'wb'))
                    countf += 1
                    for attr in self.__dict__:
                        if "_sents" in attr:
                            self.__setattr__(attr, [])

            # padding
            max_len_sent = max(map(lambda x: len(x), self.ori_token_sents))
            self.entity_ids_sents, _, max_len_entity = pad_label_sequences(self.entity_ids_sents, nlevels=2, pad_tok=config.entity_vocab.get(config.PAD), len_default_sent=max_len_sent)
            self.input_char_ids_sents, _, max_len_word = pad_label_sequences(self.input_char_ids_sents, nlevels=2, pad_tok=config.char_vocab.get(config.PAD), len_default_sent=max_len_sent)
            self.input_ids_sents, max_len_token_sent = pad_label_sequences(self.input_ids_sents, nlevels=1, pad_tok=config.word_vocab.get(config.PAD))
            self.input_mask_sents, _ = pad_label_sequences(self.input_mask_sents, pad_tok=0, len_default_sent=max_len_token_sent)
            self.input_segment_sents, _ = pad_label_sequences(self.input_segment_sents, pad_tok=0, len_default_sent=max_len_token_sent)
            self.scatter_offset_sents, _ = pad_scatter_sequences(self.scatter_offset_sents, max_ori_index=max_len_sent + 1, len_default_sent=max_len_token_sent)
            self.input_word_mask_sents, _ = pad_label_sequences(self.input_word_mask_sents, pad_tok=0, nlevels=1, len_default_sent=max_len_sent)
            self.edge_id_sents, _ = pad_label_sequences(self.edge_id_sents, pad_tok=config.edge_vocab.get(config.PAD))
            self.trigger_ids_sents, _ = pad_label_sequences(self.trigger_ids_sents, pad_tok=config.trigger_vocab.get(config.PAD), len_default_sent=max_len_sent)
            self.anchor_ids_sents, _ = pad_label_sequences(self.anchor_ids_sents, pad_tok=0)
            # dump
            if len(self.input_ids_sents) > 0:
                pickle.dump(self, open(config.reload_path + "sub_dataset_{}_{}_{}_{}.pkl".format(min_len, max_len, countf, typedt), 'wb'))

            print("event max id: ", self.trigger_ids_sents.max(), self.config.trigger_vocab_size)
            print("entity max id: ", self.entity_ids_sents.max(), self.config.entity_vocab_size)
            print("arg max id: ", max(map(max,[adj['values'] if len(adj['values']) > 0 else [0] for adj in self.arg_id_sents])), self.config.argument_vocab_size)
            print("max_len_sent: ", max_len_word)
            print("max_len_entity: ", max_len_entity)
            print("max_len_sent: ", max_len_entity)
            print("max_len_token_sent: ", max_len_token_sent)
            pass

        pass

    def __getitem__(self, idx):
        return self.input_ids_sents[idx], self.input_mask_sents[idx], self.input_segment_sents[idx], self.scatter_offset_sents[idx], \
        self.input_word_mask_sents[idx], self.input_char_ids_sents[idx], self.entity_ids_sents[idx], self.edge_id_sents[idx], self.node_node_adj_sents[idx], \
        self.node_edge_map_sents[idx], self.edge_node_map_sents[idx], self.edge_edge_adj_sents[idx], self.trigger_ids_sents[idx], self.anchor_ids_sents[idx], self.arg_id_sents[idx], idx, self.ori_token_sents[idx]


    def get_samples_weight(self, rate=None):
        samples_weight = []
        if self.multiple_label is None:
            for triggers in self.trigger_ids_sents:
                count_not_none = 0
                for trigger in triggers:
                    if trigger != self.config.trigger_vocab.get(self.config.O_tag) and trigger != self.config.trigger_vocab.get(self.config.PAD):
                        count_not_none += 1

                if count_not_none:
                    if rate is not None:
                        samples_weight.append(rate)
                    else:
                        samples_weight.append(5 * count_not_none)
                else:
                    samples_weight.append(0)
        else:
            for triggers in self.multiple_label:
                count_not_none = 0
                for grTrigger in triggers:
                    if any(trigger != self.config.trigger_vocab.get(self.config.O_tag) \
                           and trigger != self.config.trigger_vocab.get(self.config.PAD) 
                           for trigger in grTrigger):
                        count_not_none += 1

                if count_not_none:
                    if rate is not None:
                        samples_weight.append(rate)
                    else:
                        samples_weight.append(2 * count_not_none)
                else:
                    samples_weight.append(1)

        return np.array(samples_weight), len(samples_weight)

    def __len__(self):
        return len(self.input_ids_sents)
    
    
    def get_sample(self, start, end):
        input = {
            "input_ids": torch.tensor([item for item in self.input_ids_sents[start:end]]),
            "attention_mask": torch.tensor([item for item in self.input_mask_sents[start:end]]),
            "token_type_ids": torch.tensor([item for item in self.input_segment_sents[start:end]]),
            "scatter_offsets": torch.tensor([item for item in self.scatter_offset_sents[start:end]]),
            "sparse_offsets": None,
            "input_word_masks": torch.tensor(self.input_word_mask_sents[start:end]),
            "character_ids": torch.tensor([item for item in self.input_char_ids_sents[start:end]]),
            "entity_ids": torch.tensor([item for item in self.entity_ids_sents[start:end]]),
            "edge_ids": torch.tensor([item for item in self.edge_id_sents[start:end]]),
            "node_node_adj": [item for item in self.node_node_adj_sents[start:end]],
            "node_edge_map": [item for item in self.node_edge_map_sents[start:end]],
            "edge_node_map": [item for item in self.edge_node_map_sents[start:end]],
            "edge_edge_adj": [item for item in self.edge_edge_adj_sents[start:end]],
            "trigger_labels": torch.tensor([item for item in self.trigger_ids_sents[start:end]]),
            "anchor_labels": torch.tensor([item for item in self.anchor_ids_sents[start:end]]),
            "arg_labels": [item for item in self.arg_id_sents[start:end]]
        }

        return input

def pad(batch):
    return list(map(list, zip(*batch)))


if __name__ == '__main__':
    config = BaseConfig()
    ACE_DATASET(config, path="/home/thanhduc/work/ACE2005_preprocessing/output/test.json")