import collections
import glob
import json

import numpy as np
import torch

from config import label_constants
from utils import graph_utils


def generate_bert_map(path):
    data = json.load(open(path, 'r', encoding="utf8"))


def export_dependecy_vocab(path, dep_type='enhancedPlusPlusDependencies'):
    sourcefiles = glob.glob(path + "*.json")
    dep_dict = {}
    for file_ in sourcefiles:
        data = json.load(open(file_, 'r', encoding="utf8"))
        for doc_name in data:
            doc = data[doc_name]
            for sentence in doc:
                dependencies = sentence[dep_type]
                for dep in dependencies:
                    if dep['governor'] == -1: continue
                    if dep['dep'] not in dep_dict:
                        dep_dict.update({dep['dep']: len(dep_dict)})

    or_deps = list(dep_dict.keys())
    for dep in or_deps:
        dep_dict.update({dep + "_inverse": len(dep_dict)})

    json.dump(dep_dict, open("vocabs/dep_dict.json", 'w', encoding="utf8"), indent=2, ensure_ascii=False)

    pass


def generate_dependency_sparse_matrix(path, dep_type='enhancedPlusPlusDependencies', limit=None):
    data = json.load(open(path, 'r', encoding="utf8"))
    with open("vocabs/dep_dict.json", 'r') as f:
        dep_dict = json.load(f)
    max_edge = []
    for idf, file in enumerate(data):
        doc = data[file]
        for ids, sentence in enumerate(doc):
            dependencies = sentence[dep_type]
            if "Another argument" in sentence['sentence']:
                print('x')
            rows, cols = [], []
            dep_connects = {}
            for dep in dependencies:
                if dep['governor'] == -1: continue
                rows.append(dep['governor'])
                cols.append(dep['dependent'])
                dep_connects.update({"{}_{}".format(dep['governor'], dep['dependent']): dep_dict.get(dep['dep'])})

            for dep in dependencies:
                if dep['governor'] == -1: continue
                if "{}_{}".format(dep['dependent'], dep['governor']) not in dep_connects:
                    rows.append(dep['dependent'])
                    cols.append(dep['governor'])
                    dep_connects.update({"{}_{}".format(dep['dependent'], dep['governor']): dep_dict.get(dep['dep'] + "_inverse")})
                else:
                    print('x')
            indices = []
            for row, col in zip(rows, cols):
                indices.append([row, col])

            indices.sort(key=lambda x: x)
            rows, cols = [], []
            for row, col in indices:
                rows.append(row)
                cols.append(col)

            indices = [rows, cols]
            if len(rows) > 0:
                if limit is not None:
                    if limit < max(max(indices[0]), max(indices[1])):
                        new_indices = [[], []]
                        for edge_iv_id, i in enumerate(indices[0]):
                            v = indices[1][edge_iv_id]
                            if limit is not None and (v >= limit or i >= limit):
                                continue
                            else:
                                new_indices[0].append(i)
                                new_indices[1].append(v)

                        indices = new_indices

                sentence['adj'] = {
                    "indices": indices,
                    "values": [1 for _ in range(len(rows))]
                }

                adj, edge_id_dict = graph_utils.extract_edge_adj(indices, type_edge="direct")
                max_edge.append(len(edge_id_dict))
                values = []
                for row, col in zip(*indices):
                    values.append(dep_connects.get("{}_{}".format(row, col)))

                sentence['edge_ids'] = values

                # print(adj.to_dense())
                sentence['edge_adj'] = {
                    "indices": adj.coalesce().indices().tolist(),
                    "values": adj.coalesce().values().tolist()
                }

                adj = graph_utils.extract_edge_map(indices, edge_id_dict, type_edge="direct")
                # print(adj.to_dense())
                sentence['node_edge_map'] = {
                    "indices": adj.coalesce().indices().tolist(),
                    "values": adj.coalesce().values().tolist()
                }

                adj = graph_utils.extract_edge_map_v2(indices, edge_id_dict, type_edge="direct")
                # print(adj.to_dense())
                sentence['node_edge_map_v2'] = {
                    "indices": adj.coalesce().indices().tolist(),
                    "values": adj.coalesce().values().tolist()
                }

                adj = graph_utils.extract_node_map(indices, edge_id_dict, type_edge="direct")
                # print(adj.to_dense())
                sentence['node_map'] = {
                    "indices": adj.coalesce().indices().tolist(),
                    "values": adj.coalesce().values().tolist()
                }

                adj = graph_utils.extract_node_map_v2(indices, edge_id_dict, type_edge="direct")
                # print(adj.to_dense())
                sentence['node_map_v2'] = {
                    "indices": adj.coalesce().indices().tolist(),
                    "values": adj.coalesce().values().tolist()
                }

            # break

        # break
    json.dump(data, open(path, 'w', encoding="utf8"), indent=2, ensure_ascii=False)
    enumerate_data = json.load(open("vocabs/enumerate_candidate_length.json", 'r', encoding="utf8"))
    enumerate_data['max_edge'] = max(max_edge)
    enumerate_data['max_edge_count'] = collections.Counter(max_edge)

    json.dump(enumerate_data, open("vocabs/enumerate_candidate_length.json", 'w', encoding="utf8"), indent=2)

def generate_argument_label(path):
    arg_dict = {"[PAD]": 1}
    for arg in label_constants.ARGUMENTS:
        arg_dict.update({"B-" + arg: len(arg_dict) + 1})
        arg_dict.update({"I-" + arg: len(arg_dict) + 1})

    data = json.load(open(path, 'r', encoding="utf8"))
    for file in data:
        doc = data[file]
        for sentence in doc:
            rows, cols, values = [], [], []
            pairs = []
            for event in sentence['golden-event-mentions']:
                event_trigger = list(range(event['trigger']['start'], event['trigger']['end']))
                if event['trigger']['start'] >= 0:
                    sorted_arg = sorted(event['arguments'], key=lambda x: len(x['extent-text']))
                    for arg in sorted_arg:
                        if any(token in arg['role'] for token in ["1", "2", "3"]):
                            print('x')

                        prefix = "B-"
                        for id_, arg_id in enumerate(list(range(arg['start'], arg['end']))):
                            if "{}_{}".format(event['trigger']['start'], arg_id) not in pairs:
                                rows.append(event['trigger']['start'])
                                cols.append(arg_id)
                                pairs.append("{}_{}".format(event['trigger']['start'], arg_id))
                                values.append(prefix + arg['role'])
                                if prefix == "B-":
                                    prefix = "I-"

                            else:
                                print(arg['text'], arg['extent-text'])


            indices = [rows, cols]
            sentence['arg_map'] = {
                "indices": indices,
                "values": values
            }

    json.dump(data, open(path, 'w', encoding="utf8"), indent=2, ensure_ascii=False)


def enumerate_candidate_size(path, file=None, is_save=False):
    sourcefiles = glob.glob(path + "*.json")
    dep_dict = {}
    event_enumerate = []
    arg_enumerate = []
    entity_enumerate = []
    event_text = dict()
    len_sent = []
    len_word = []
    len_sent_has_event = []
    max_entity = 0
    max_id_event = []
    if file is not None:
        sourcefiles = [file]

    for file_ in sourcefiles:
        print(file_)
        data = json.load(open(file_, 'r', encoding="utf8"))
        for doc_name in data:
            doc = data[doc_name]
            for sentence in doc:
                tokens = dict(zip(list(range(len(sentence['words']))), [[] for _ in range(len(sentence['words']))]))
                len_sent.append(len(tokens))
                len_word.extend([len(word) for word in sentence['words']])
                if len(sentence['golden-event-mentions']) > 0:
                    len_sent_has_event.append(len(tokens))
                for event in sentence['golden-event-mentions']:
                    if event['trigger']['end'] - event['trigger']['start'] <= 0:
                        if event['trigger']['start'] >= 0:
                            if event['trigger']['end'] - event['trigger']['start'] < 0:
                                print("event: ", event['trigger']['start'], event['trigger']['end'],
                                      event['trigger']['text'], " - ", sentence['words'])

                            event['trigger']['end'] = event['trigger']['start'] + len(event['trigger']['text'].split())
                        else:
                            continue
                            pass
                            # print("event: ", event['trigger']['text'], " - ", sentence['words'])

                    if event['trigger']['text'] not in event_text:
                        event_text.update({event['trigger']['text']: {event['event_type']: 1}})

                    else:
                        if event['event_type'] not in event_text[event['trigger']['text']]:
                            event_text[event['trigger']['text']].update({event['event_type']: 1})
                        else:
                            event_text[event['trigger']['text']][event['event_type']] += 1

                    event_enumerate.append(event['trigger']['end'] - event['trigger']['start'])
                    max_id_event.append(event['trigger']['end'])
                    if event['trigger']['end'] > 50:
                        print(doc_name, event['trigger']['text'], " -> ", " ".join(word for word in sentence['words']))

                    for arg in event['arguments']:
                        if arg['end'] - arg['start'] <= 0:
                            if arg['start'] >= 0:
                                if arg['end'] - arg['start'] < 0:
                                    print("arg: ", arg['start'], arg['end'], arg['text'], " - ", sentence['words'])

                                arg['end'] = arg['start'] + len(arg['text'].split())
                            else:
                                continue
                                pass
                                # print("arg: ", arg['text'], " - ", sentence['words'])

                        arg_enumerate.append(arg['end'] - arg['start'])

                for entity in sentence['golden-entity-mentions']:
                    if entity['end'] - entity['start'] <= 0:
                        if entity['start'] >= 0:
                            if entity['end'] - entity['start'] < 0:
                                print("entity: ", entity['start'], entity['end'], entity['text'], " - ",
                                      sentence['words'])

                            entity['end'] = entity['start'] + len(entity['text'].split())
                        else:
                            continue
                            pass
                            # print("entity: ", entity['text'], " - ", sentence['words'])

                    prefix = "B-"
                    for tid in range(entity['start'], entity['end']):
                        if tid in tokens:
                            tokens[tid].append(prefix + entity['entity-type'])
                            if prefix == "B-":
                                prefix = "I-"

                    entity_enumerate.append(entity['end'] - entity['start'])

                sentence['entities'] = list(tokens.values())
                if len(tokens) > 0:
                    max_entity = max(max_entity, max(map(len, list(tokens.values()))))
        if is_save:
            json.dump(data, open(file_, 'w', encoding="utf8"), indent=2, ensure_ascii=False)

    print("max overlap entity: ", max_entity)
    countevent = dict(collections.Counter(event_enumerate))
    countentity = dict(collections.Counter(entity_enumerate))
    countarg = dict(collections.Counter(arg_enumerate))
    json.dump(
        {
            "max_id_event": max(max_id_event),
            "max_overlap_entity_len": max_entity,
            "max_sent_len": max(len_sent),
            "max_word": max(len_word),
            "max_id_event_count": collections.Counter(max_id_event),
            "max_sent_has_event": max(len_sent_has_event),
            "len_word_count": collections.Counter(len_word),
            "len_sent_count": collections.Counter(len_sent),
            "len_sent_has_event_count": collections.Counter(len_sent_has_event),
            "event_count": countevent,
            "entity_count": countentity,
            "arg_count": countarg,
            "event_candidate": event_text
        }, open("vocabs/enumerate_candidate_length.json", 'w', encoding="utf8"), indent=2, ensure_ascii=False)


def _pad_label_sequences(sequences, pad_tok=None, max_length=None):
    """
    Args:
        sequences: a generator of list or tuple
        pad_tok: the char to pad with

    Returns:
        a list of list where each sublist has same length
    """
    assert max_length is not None

    sequence_padded, sequence_length = [], []

    for ids, seq in enumerate(sequences):
        seq = list(seq)
        if pad_tok is not None:
            seq_ = seq[:max_length] + [pad_tok]*max(max_length - len(seq), 0)
        else:
            pad_tok_ = seq[-1]

             # take the last element in seq as the pad_token
             #(deal with multiple label when scatter index label to onehot matrix target)
            seq_ = seq[:max_length] + [pad_tok_ for offset in range(max(max_length - len(seq), 0))]

        sequence_padded +=  [seq_]

    return sequence_padded


def pad_label_sequences(sequences, pad_tok, nlevels=1, len_default_sent=None, len_default_word=None):
    """
    Args:
        sequences: a generator of list or tuple
        pad_tok: the char to pad with
        nlevels: "depth" of padding, for the case where we have characters ids

    Returns:
        a list of list where each sublist has same length

    """

    if len_default_sent is not None:
        max_length_sentence = len_default_sent
    else:
        max_length_sentence = max(map(lambda x: len(x), sequences))

    if len_default_word is not None:
        max_length_word = len_default_word
    elif nlevels == 2:
        max_length_word = max([max(map(lambda x: len(x), seq)) for seq in sequences])

    if nlevels == 1:
        sequence_padded = _pad_label_sequences(sequences,
                                               pad_tok,
                                               max_length_sentence)
        return np.asarray(sequence_padded), max_length_sentence

    elif nlevels == 2:
        sequence_padded, sequence_length = [], []
        for seq in sequences:
            # all entities per words are same length now
            # take the last real element in list as the pad_tok
            sp = _pad_label_sequences(seq,
                                      pad_tok=pad_tok,
                                      max_length=max_length_word)
            sequence_padded += [sp]

        sequence_padded = _pad_label_sequences(sequence_padded,
                                               [pad_tok]*max_length_word,
                                               max_length_sentence)

        return np.asarray(sequence_padded), max_length_sentence, max_length_word


def pad_scatter_sequences(sequences, max_ori_index, len_default_sent=None):
    padded = []
    for seq in sequences:
        pad_tok_ = seq[-1]
        seq_ = seq[:len_default_sent] + [min(pad_tok_ + offset + 1, max_ori_index) for offset in range(max(len_default_sent - len(seq), 0))]
        padded.append(seq_)

    return padded, len_default_sent
    pass

def refine_position_from_extent(arg, words):
    extent_text = arg['extent-text']

if __name__ == '__main__':
    # enumerate_candidate_size(path="/home/thanhduc/work/ACE2005_preprocessing/output/",
    #                          file="/home/thanhduc/work/ACE2005_preprocessing/output/dev.json",
    #                          is_save=True)

    # generate_dependency_sparse_matrix("/home/thanhduc/work/ACE2005_preprocessing/output/train.json", dep_type='basicDependencies')
    # export_dependecy_vocab(path="/home/thanhduc/work/ACE2005_preprocessing/output/")

    generate_argument_label(path="/home/thanhduc/work/ACE2005_preprocessing/output/train.json")
    generate_argument_label(path="/home/thanhduc/work/ACE2005_preprocessing/output/test.json")
    # a = [2,1,2,1,2,1,1,4,5]
    # b = collections.Counter(a)
    # print(b)
    pass
