import json
import os.path

import torch
from config import label_constants as constant
from models.cnn.tokenizer import Tokenizer


class BaseConfig:
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # data path
    root_data_path = "data/"
    dep_path = root_data_path + "dep_dict.json"
    char_path = root_data_path + "chars.txt"
    parse_path = root_data_path + ""

    reload_path = root_data_path + "dataset/"

    # output path
    save_path = "results"


    # layer dim
    entity_embed_dim = 10
    character_embed_dim = 10
    edge_embed_dim = 10
    hidden_dim = 30
    out_dim = 30
    out_edge_dim = 30
    num_heads = 2
    attn_dim = 30

    # tag
    PAD = constant.PAD
    O_tag = constant.NONE
    UNK = constant.UNK
    max_k_hops = 5

    # vocab size
    word_vocab_size = 20



    # training
    dropout = 0.3
    layer_norm_eps = 12e-8
    batch_size = 5
    test_batch_size = 5
    eval_batch_size = 5
    epoch = 5
    weight_decay = 1e-5
    weight_decay_bert = 5e-5
    learning_rate = 5e-3
    adam_epsilon = 0.8
    id_model = 1
    max_restart=2




    # input_size
    seq_max_len = 143
    word_max_len = 15,
    group_entity_max_len = 4
    edge_max_len = 284 # 122-test 224-dev 284-train
    model = './bert-base-cased'
    layer_pooling_strategy: str = "last"
    subword_pooling_strategy: str = "scatter"
    pooling_mode = "mean"
    output_layers = (-4, -3, -2, -1)
    fine_tune = True
    return_all = False
    activation_layer = None # torch.nn.Linear(out_dim, out_dim)
    dropout: float = 0.1
    bias: bool = True

    def __init__(self):
        self.build_vocab()

    def show_config(self):
        for key, value in self.__dict__.items():
            print("{}: {}".format(key, value))


    def build_vocab(self):
        self.trigger_vocab = {}
        self.argument_vocab = {}
        self.entity_vocab = {}
        self.edge_vocab = {}
        self.char_vocab = {}
        self.word_vocab = {}

        self.trigger_vocab.update({constant.PAD: 0, constant.NONE: 1})
        for label in constant.TRIGGERS:
            self.trigger_vocab.update({"B-" + label: len(self.trigger_vocab)})
            self.trigger_vocab.update({"I-" + label: len(self.trigger_vocab)})

        self.id2trigger_vocab = dict(zip(self.trigger_vocab.values(), self.trigger_vocab.keys()))

        self.argument_vocab.update({constant.PAD: 0, constant.NONE: 1})
        for label in constant.ARGUMENTS:
            self.argument_vocab.update({"B-" + label: len(self.argument_vocab)})
            self.argument_vocab.update({"I-" + label: len(self.argument_vocab)})

        self.id2argument_vocab = dict(zip(self.argument_vocab.values(), self.argument_vocab.keys()))

        self.entity_vocab.update({constant.PAD: 0, constant.NONE: 1})
        for label in constant.ENTITIES:
            self.entity_vocab.update({"B-" + label: len(self.entity_vocab)})
            self.entity_vocab.update({"I-" + label: len(self.entity_vocab)})

        self.id2entity_vocab = dict(zip(self.entity_vocab.values(), self.entity_vocab.keys()))

        self.edge_vocab.update({constant.PAD: 0})
        for label in json.load(open(self.dep_path, 'r')):
            self.edge_vocab.update({label: len(self.edge_vocab)})

        self.id2edge_vocab = dict(zip(self.edge_vocab.values(), self.edge_vocab.keys()))

        self.char_vocab.update({constant.PAD: 0})
        self.char_vocab.update({constant.UNK: 1})
        with open(self.char_path, 'r') as f:
            data = [row for row in f.read().split("\t") if row.strip() != ""]

        for label in data:
            self.char_vocab.update({label: len(self.char_vocab)})

        # vocab size
        self.word_vocab_size = len(self.word_vocab)
        self.char_vocab_size = len(self.char_vocab)
        self.edge_vocab_size = len(self.edge_vocab)
        self.entity_vocab_size = len(self.entity_vocab)
        self.trigger_vocab_size = len(self.trigger_vocab)
        self.argument_vocab_size = len(self.argument_vocab)

        json.dump(self.trigger_vocab, open(os.path.join(self.save_path, "trigger_vocab.json"), 'w'), indent=2)
        json.dump(self.argument_vocab, open(os.path.join(self.save_path, "arg_vocab.json"), 'w'), indent=2)
        json.dump(self.entity_vocab, open(os.path.join(self.save_path, "entity_vocab.json"), 'w'), indent=2)
        json.dump(self.edge_vocab, open(os.path.join(self.save_path, "edge_vocab.json"), 'w'), indent=2)
        json.dump(self.char_vocab, open(os.path.join(self.save_path, "char_vocab.json"), 'w'), indent=2)

        tokenizer = Tokenizer("bert-base-cased", return_sparse_offsets=False)
        self.word_vocab = tokenizer.huggingface_tokenizer.get_vocab()
        self.id2word = dict(zip(self.word_vocab.values(), self.word_vocab.keys()))

class GraphConfig(BaseConfig):
    pass


if __name__ == '__main__':
    BaseConfig()