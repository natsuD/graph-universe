import copy
import pickle

import torch

from config.config import BaseConfig
from dataset_processing.ace_dataset import ACE_DATASET
from dataset_processing.data_utils import pad_label_sequences, pad_scatter_sequences
from models.cnn.tokenizer import Tokenizer
from models.cnn.embedder import TransformersEncoder, TransformersEmbedder
from models.model import EventModel

# tokenizer = Tokenizer("./bert-base-cased", return_sparse_offsets=False)
#
# text = ["This is a sampless sentence 2 abc".split()]
# a = tokenizer(text, is_split_into_words=True)
# # a['scatter_offsets'], _ = pad_scatter_sequences(a['scatter_offsets'], max_ori_index=39 + 2, len_default_sent=50)
# print(a)
#
#
# for key_ in a:
#     a[key_] = torch.tensor(a[key_])
#
# scatter = copy.deepcopy(a['scatter_offsets'])
# del a['scatter_offsets']
# del a['sentence_lengths']
import transformers as tr

model_name = "./bert-base-cased"
# config = tr.AutoConfig.from_pretrained(
#                 model_name,
#                 output_hidden_states=True,
#                 output_attentions=True
#             )
#
# model = tr.AutoModel.from_pretrained(
#                     model_name, config=config
#                 )

# model = TransformersEmbedder(model_name, pooling_mode="sum")
#
# res = model(**a)
# loss = torch.nn.CrossEntropyLoss()
# # out = torch.zeros(1, 9, 768)
# # out.scatter_reduce_(1, scatter.unsqueeze(-1).expand_as(res.last_hidden_state), res.last_hidden_state, reduce="mean")
# # logit = torch.nn.Linear(768, 3)(out)
# # logit = torch.nn.Linear(768, 3)(res.last_hidden_state)
# logit = torch.nn.Linear(768, 3)(res.word_embeddings)
# loss = loss(logit.contiguous().view(-1, 3), torch.randint(0,3, size=(9,)))
# loss.backward()
# print(loss)

config = BaseConfig()
model = EventModel(config)

data = ACE_DATASET(BaseConfig(),
                   typedt="dev",
                   path="/home/thanhduc/work/ACE2005_preprocessing/output/dev.json",
                   rebuild=True,
                   partial=50)

data = ACE_DATASET(BaseConfig(),
                   typedt="dev",
                   path="/home/thanhduc/work/ACE2005_preprocessing/output/dev.json",
                   rebuild=True,
                   min_len=40,
                   max_len=200,
                   partial=50)

# data = ACE_DATASET(BaseConfig(), typedt="dev", path="sub_dataset_dev.pkl")

# print(data.__len__())
# inputs = {
#     "input_ids": torch.randint(1, 99, size=(1,4)),
#     "attention_mask": torch.tensor([[1,1,1,1]]),
#     "token_type_ids": torch.tensor([[0, 0, 0, 0]]),
#     "scatter_offsets": torch.tensor([[0, 1, 2, 3]]),
#     "sparse_offsets": None,
#     "character_ids":  torch.randint(1,10, size=(1, 4, 3)),
#     "entity_ids":  torch.randint(1,20, size=(1,4,3)),
#     "edge_ids": torch.randint(1, 11, size=(1,4)),
#     "node_node_adj": [{"indices": [[0,0,1,3,3],[1,0,0,1,3]], "values":[1,1,1,1,1]}],
#     "node_edge_map": [{"indices": [[0,0,1,3,3],[1,0,0,1,3]], "values":[1,3,1,2,1]}],
#     "edge_node_map": [{"indices": [[0,0,1,3,3,1],[1,0,0,1,3,3]], "values":[1,1,3,2,1,2]}],
#     "edge_edge_adj": [{"indices": [[0,0,1,3,3],[1,0,0,1,3]], "values":[1,1,1,1,1]}],
#     "trigger_labels": torch.tensor([[1,3,2,1]]),
#     "anchor_labels": torch.tensor([[0, 1, 1, 0]]),
#     "arg_labels": [{"indices": [[0,0,1,3,3],[1,0,0,1,3]], "values":[1,1,1,1,1]}],
#     "arg_mask": torch.randint(0,2, size=(1,4,4)),
# }
#
# inputs = data.get_sample(162, 165) # 163
# loss = model(**inputs)
# loss[0].backward()



