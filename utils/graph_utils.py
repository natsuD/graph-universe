import torch


def extract_edge_adj(indices, type_edge="undirect"):
    """

    :param indices:
    :param type_edge:
    :return: [num_edge, num_edge]
    """
    print("----------export_edge_adj")
    edge_index = indices
    row = []
    col = []

    edge_id_dict = dict()
    edge_pairs = []
    current_id = 0

    for edge_iv_id, i in enumerate(edge_index[0]):
        v = edge_index[1][edge_iv_id]

        if type_edge == "undirect":
            if "{}_{}".format(i, v) not in edge_id_dict and "{}_{}".format(v, i) not in edge_id_dict:
                edge_id_dict.update({"{}_{}".format(i, v): len(edge_id_dict)})
        else:
            if "{}_{}".format(i, v) in edge_id_dict:
                print('x')
            edge_id_dict.update({"{}_{}".format(i, v): len(edge_id_dict)})

    for edge_iv_id, i in enumerate(edge_index[0]):
        v = edge_index[1][edge_iv_id]
        for edge_vu_id, j in enumerate(edge_index[0]):
            u = edge_index[1][edge_vu_id]
            if type_edge == "undirect":
                if "{}_{}".format(i, v) in edge_id_dict or "{}_{}".format(v, i) in edge_id_dict:
                    edge_iv_id = edge_id_dict.get("{}_{}".format(i, v), edge_id_dict.get("{}_{}".format(v, i)))
                else:
                    edge_id_dict.update({"{}_{}".format(i, v): current_id})
                    edge_iv_id = current_id
                    current_id += 1

                if "{}_{}".format(j, u) in edge_id_dict or "{}_{}".format(u, j) in edge_id_dict:
                    edge_vu_id = edge_id_dict.get("{}_{}".format(j, u), edge_id_dict.get("{}_{}".format(u, j)))
                else:
                    edge_id_dict.update({"{}_{}".format(j, u): current_id})
                    edge_vu_id = current_id
                    current_id += 1
            else:
                edge_vu_id = edge_id_dict.get("{}_{}".format(j, u))
                edge_iv_id = edge_id_dict.get("{}_{}".format(i, v))

            if (edge_vu_id != edge_iv_id and (j == v or u == i)):  # edge in / edge out
                if "{}_{}".format(edge_iv_id, edge_vu_id) in edge_pairs:
                    continue
                else:
                    edge_pairs.append("{}_{}".format(edge_iv_id, edge_vu_id))

                row.append(edge_iv_id)
                col.append(edge_vu_id)

    if len(row + col) == 0: return None, None
    num_edge = max(row + col) + 1
    indices_ = [row, col]
    sparse_adj = torch.sparse_coo_tensor(indices=indices_, values=[1] * len(indices_[0]), size=(num_edge, num_edge))
    # print(sparse_adj.to_dense())
    # print(len(row))

    # extract_edge_map_v2(indices, edge_id_dict, type_edge)
    # extract_node_map_v2(indices, edge_id_dict, type_edge)

    return sparse_adj, edge_id_dict


def extract_edge_map(indices, edge_dict=None, type_edge="undirect"):
    """
    extract edge connect 2 node
    :param indices:
    :param edge_dict:
    :param type_edge:
    :return: [num_node, num_edge]
    """
    print("----------export_edge map")
    # indices = indices.numpy()
    try:
        num_node = max(max(indices[0]), max(indices[1])) + 1
    except Exception as e:
        print('x')

    rows = []
    cols = []
    pair = []
    values = []
    for i_, row in enumerate(indices[0]):
        col = indices[1][i_]
        if type_edge == "undirect":
            if "{}_{}".format(row, col) in edge_dict or "{}_{}".format(col, row) in edge_dict:
                temp_id = edge_dict.get("{}_{}".format(row, col), edge_dict.get("{}_{}".format(col, row)))
            else:
                edge_dict.update({"{}_{}".format(row, col): len(edge_dict)})
                temp_id = edge_dict.get("{}_{}".format(row, col), edge_dict.get("{}_{}".format(col, row)))
        else:
            if "{}_{}".format(row, col) in edge_dict:
                temp_id = edge_dict.get("{}_{}".format(row, col))
            else:
                edge_dict.update({"{}_{}".format(row, col): len(edge_dict)})
                temp_id = edge_dict.get("{}_{}".format(row, col))

        if type_edge == "undirect":
            if "{}_{}".format(row, temp_id) not in pair:
                rows.extend([row])
                cols.extend([temp_id])
                pair.append("{}_{}".format(row, temp_id))

        if "{}_{}".format(col, temp_id) not in pair:
            rows.extend([col])
            cols.extend([temp_id])
            pair.append("{}_{}".format(col, temp_id))

        sparse_edge_map = torch.sparse_coo_tensor(indices=[rows, cols], values=[1 for _ in rows], size=(num_node, len(edge_dict.values())))

    return sparse_edge_map

    pass


def extract_edge_map_v2(indices, edge_dict=None, type_edge="undirect"):
    """
    extract edge connect 2 node
    :param indices:
    :param edge_dict:
    :param type_edge:
    :return: [num_node, num_node]
    """
    print("----------export_edge map")
    # indices = indices.numpy()
    try:
        num_node = max(max(indices[0]), max(indices[1])) + 1
    except Exception as e:
        print('x')

    rows = []
    cols = []
    if type_edge == "direct":
        sparse_edge_map = torch.sparse_coo_tensor(indices=indices, values=list(edge_dict.values()),
                                                  size=(num_node, num_node))
    else:
        pair = []
        values = []
        current_edge_id = 0
        for i_, row in enumerate(indices[0]):
            col = indices[1][i_]
            if "{}_{}".format(row, col) not in pair and "{}_{}".format(col, row) not in pair:
                if edge_dict is not None:
                    temp_id = edge_dict.get("{}_{}".format(row, col), edge_dict.get("{}_{}".format(col, row)))
                    if temp_id is not None:
                        current_edge_id = temp_id
                    else:
                        print('x')
                pair.append("{}_{}".format(row, col))
                rows.extend([row, col])
                cols.extend([col, row])
                values.extend([current_edge_id + 1] * 2)
                current_edge_id += 1

        sparse_edge_map = torch.sparse_coo_tensor(indices=[rows, cols], values=values, size=(num_node, num_node))

    # print(sparse_edge_map.to_dense())
    # print(sparse_edge_map.to_dense().to_sparse())
    return sparse_edge_map

    pass


def extract_node_map(indices, edge_dict={}, type_edge="undirect"):
    """
    connect node
    :param indices:
    :return: [num_edge, num_node]
    """
    print("----------export_node map v1")
    rows = []
    cols = []

    edge_index = indices
    values = []
    edge_id_dict = dict()
    node_pairs = []
    for edge_iv_id, i in enumerate(edge_index[0]):
        v = edge_index[1][edge_iv_id]
        if type_edge == "undirect":
            if "{}_{}".format(i, v) in edge_id_dict or "{}_{}".format(v, i) in edge_id_dict:
                edge_iv_id = edge_id_dict.get("{}_{}".format(i, v), edge_id_dict.get("{}_{}".format(v, i)))
            else:
                edge_id_dict.update({"{}_{}".format(i, v): len(edge_id_dict)})
                edge_iv_id = edge_id_dict.get("{}_{}".format(i, v))

        else:
            if "{}_{}".format(i, v) in edge_id_dict:
                edge_iv_id = edge_id_dict.get("{}_{}".format(i, v))
            else:
                edge_id_dict.update({"{}_{}".format(i, v): len(edge_id_dict)})
                edge_iv_id = edge_id_dict.get("{}_{}".format(i, v))

        if type_edge == "undirect":
            if "{}_{}".format(i, v) in node_pairs or "{}_{}".format(v,i) in node_pairs:
                continue

        rows.extend([edge_iv_id, edge_iv_id])
        cols.extend([i, v])


    if len(rows + cols) == 0: return None
    indices = [rows, cols]
    num_edge = max(rows) + 1
    num_node = max(cols) + 1
    sparse_node_map = torch.sparse_coo_tensor(indices=indices, values=len(rows) * [1], size=(num_edge, num_node))

    # print(sparse_node_map.to_dense())
    # print(len(rows))
    return sparse_node_map

    pass


def extract_node_map_v2(indices, edge_id_dict={}, type_edge="direct"):
    """
    connect node
    :param indices:
    :param type_edge:
    :return: [num_edge, num_edge]
    """
    print("----------export_node map v2")

    rows = []
    cols = []

    edge_index = indices
    values = []
    edge_pairs = []
    current_id = 0
    for edge_iv_id, i in enumerate(edge_index[0]):
        v = edge_index[1][edge_iv_id]
        for edge_vu_id, j in enumerate(edge_index[0]):
            u = edge_index[1][edge_vu_id]
            if type_edge == "undirect":
                if "{}_{}".format(i, v) in edge_id_dict or "{}_{}".format(v, i) in edge_id_dict:
                    edge_iv_id = edge_id_dict.get("{}_{}".format(i, v), edge_id_dict.get("{}_{}".format(v, i)))
                else:
                    edge_id_dict.update({"{}_{}".format(i, v): current_id})
                    edge_iv_id = current_id
                    current_id += 1

                if "{}_{}".format(j, u) in edge_id_dict or "{}_{}".format(u, j) in edge_id_dict:
                    edge_vu_id = edge_id_dict.get("{}_{}".format(j, u), edge_id_dict.get("{}_{}".format(u, j)))
                else:
                    edge_id_dict.update({"{}_{}".format(j, u): current_id})
                    edge_vu_id = current_id
                    current_id += 1
            else:
                if "{}_{}".format(i, v) in edge_id_dict:
                    edge_iv_id = edge_id_dict.get("{}_{}".format(i, v))
                else:
                    edge_id_dict.update({"{}_{}".format(i, v): current_id})
                    edge_iv_id = current_id
                    current_id += 1

                if "{}_{}".format(j, u) in edge_id_dict:
                    edge_vu_id = edge_id_dict.get("{}_{}".format(j, u))
                else:
                    edge_id_dict.update({"{}_{}".format(j, u): current_id})
                    edge_vu_id = current_id
                    current_id += 1

            if (edge_vu_id != edge_iv_id and (j == v or u == i)):  # edge in / edge out
                if "{}_{}".format(edge_iv_id, edge_vu_id) in edge_pairs:
                    continue
                else:
                    edge_pairs.append("{}_{}".format(edge_iv_id, edge_vu_id))

                rows.append(edge_iv_id)
                cols.append(edge_vu_id)
                if u == i:
                    values.append(u + 1)
                else:
                    values.append(v + 1)


    if len(rows + cols) == 0: return None

    indices = [rows, cols]
    num_edge = max(rows + cols) + 1
    sparse_node_map = torch.sparse_coo_tensor(indices=indices, values=values, size=(num_edge, num_edge))

    # print(sparse_node_map.to_dense())
    # print(len(rows))
    return sparse_node_map



if __name__ == '__main__':
    # a = torch.sparse_coo_tensor(indies=[[1,2,0,1], [0, 1, 1,0]],
    #                             values=[1,1,1,1])
    #
    a = torch.randn(3, 3)
    a = torch.where(a > 0, torch.ones_like(a), torch.zeros_like(a))
    a = torch.tensor([[1., 1., 0],
                      [1., 0., 0],
                      [1., 1., 0]])
    a = torch.tensor([[0., 1., 0., 0., 0.],
                      [1., 0., 1., 0., 0.],
                      [0., 1., 0., 1., 1.],
                      [0., 0., 1., 0., 0.],
                      [0., 0., 1., 0., 0.]])

    print(a.shape)
    print(a)
    a = a.to_sparse_coo().indices()
    print(a)
    print(extract_edge_adj(a, type_edge="undirect"))
    print("------")
    # print(extract_edge_map(a, type_edge="undirect"))
    # print("------")
    # print(extract_node_map_v2(a, type_edge="direct"))
