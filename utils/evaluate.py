# @title evaluate func not include arg
import collections

import numpy as np
import torch
from torch.utils.data import SequentialSampler, DataLoader
from tqdm import notebook

from config.config import BaseConfig
from dataset_processing.ace_dataset import pad


def find_triggers(labels, ori_words, config):
    """

    :param labels: ['B-Conflict:Attack', 'I-Conflict:Attack', 'O', 'B-Life:Marry']
    :param input_ids: seq_len, [cls_id, 12,2,4,1,..., sep_id, pad_id,...]
    :return: [(0, 2, 'Conflict:Attack'), (3, 4, 'Life:Marry')]
    """
    segs = {}
    type_trigger = ""
    st, ed = -1, -1
    for i, (x, wtoken) in enumerate(zip(labels, ori_words)):
        # refine label predict for subword
        if wtoken.startswith('##'):
            temp_idx = i - 1
            while (ori_words[temp_idx].startswith('##') and temp_idx >= 1):
                temp_idx -= 1
            if labels[temp_idx] != 'O':
                labels[i] = 'I-' + labels[temp_idx][2:]
            else:
                labels[i] = 'O'

    for i, (x, wtoken) in enumerate(zip(labels, ori_words)):
        if x.startswith("B-"): # start with B-
            if type_trigger == "":
                type_trigger = x[2:]
                st = i
            else:  # take new B-
                ed = i
                segs[st] = (ed, type_trigger)
                type_trigger = x[2:]
                st = i
        elif x.startswith("I-"):
            if type_trigger == "": # start group with I- -> convert to B-
                labels[i] = "B" + labels[i][1:]
                type_trigger = x[2:]
                st = i
            else:
                if type_trigger != x[2:]:  # I- of difference trigger type with prior trigger token
                    ed = i
                    segs[st] = (ed, type_trigger)
                    labels[i] = "B" + labels[i][1:]
                    type_trigger = x[2:]
                    st = i
        else:
            ed = i  # take O, end current group trigger
            if type_trigger != "":
                segs[st] = (ed, type_trigger)
            type_trigger = ""

    if type_trigger != "":
        segs[st] = (i, type_trigger)

    results = []
    for st in segs:
        results.append((st, *segs[st]))

    return results

def calc_metric(y_true, y_pred):
    """
    :param y_true: [(tuple), ...]
    :param y_pred: [(tuple), ...]
    :return:
    """
    num_proposed = len(y_pred)
    num_gold = len(y_true)

    y_true_set = set(y_true)
    num_correct = 0
    for item in y_pred:
        if item in y_true_set:
            num_correct += 1

    print('proposed: {}\tcorrect: {}\tgold: {}'.format(num_proposed, num_correct, num_gold))

    if num_proposed != 0:
        precision = num_correct / num_proposed
    else:
        precision = 1.0

    if num_gold != 0:
        recall = num_correct / num_gold
    else:
        recall = 1.0

    if precision + recall != 0:
        f1 = 2 * precision * recall / (precision + recall)
    else:
        f1 = 0

    return precision, recall, f1, num_gold, num_proposed, num_correct

dcount,dcount2=  {}, {}
def checkChunk(trigger_target, trigger_predict, argument_target, argument_predict, ori_sents, config, check_result=None):
    # compare trigger_target set with trigger_predict set have printing all missing label

    id2event = dict(zip(config.trigger_vocab.values(), config.trigger_vocab.keys()))
    correct_preds, total_correct, total_preds = 0., 0., 0.
    totals = []
    ground_trues = []

    triggers_pred, triggers_true, arguments_true, arguments_pred = [], [], [], []
    if check_result is None:
        fout = open('results/log_predict_{}.txt'.format(config.id_model), 'w')
    for ids, (words, lab_pred, lab) in enumerate(zip(ori_sents, trigger_predict, trigger_target)):

        lab_pred = [id2event[ide] for ide in lab_pred]
        lab = [id2event[ide] if ide != -100 else '[PAD]' for ide in lab]
        lab_chunks = set(find_triggers(lab, words, config))
        # trigger proces
        for poslab in lab_chunks:
            triggers_true.append((ids, poslab[0], poslab[2]))
            ground_trues.append(poslab[2])

        lab_pred_chunks = set(find_triggers(lab_pred, words, config))
        for poslab in lab_pred_chunks:
            triggers_pred.append((ids, poslab[0], poslab[2]))
            totals.append(poslab[2])

        if check_result is None:
            for idw, (w, t, t_h) in enumerate(zip(words, lab, lab_pred)):
                fout.write('{}\t{}\t{}\t{}\n'.format(idw, w, t, t_h))

            fout.write("\n\n")

        if check_result == 'pred2ori':
            for ori in lab_pred_chunks:
                if ori not in list(lab_chunks & lab_pred_chunks):
                    # if ori[0] in ['Attack','Meet','Transport','Transfer-Ownership','Start-Position']:
                    check = False
                    for pred in lab_chunks:
                        if pred[0] == ori[0]:
                            print('-->check fault: ', ori, '--', pred)
                            print('   ', lab_chunks)
                            print('   ', lab_pred_chunks)
                            trigger_word = ""
                            for i in range(ori[0], ori[1]):
                                trigger_word += words[i] + ' '
                            print('   trigge word: ', trigger_word)

                            full_sentence = " ".join(word for word in words)

                            print('   full sentence: ', full_sentence)
                            check = True
                            break
                    if not check:
                        print('-->check fault: ', ori, '--')
                        print('   ', lab_chunks)
                        print('   ', lab_pred_chunks)
                        trigger_word = ""
                        for i in range(ori[0], ori[1]):
                            trigger_word += words[i] + ' '
                        print('   Trigger word: ', trigger_word)

                        full_sentence = " ".join(word for word in words)
                        print('   full sentence: ', full_sentence)

        elif check_result == 'ori2pred':
            for w in lab_pred_chunks:
                dcount[w[2]] += 1

            for w in lab_chunks:
                dcount2[w[2]] += 1

            for ori in lab_chunks:
                if ori not in list(lab_chunks & lab_pred_chunks):
                    # if ori[0] in ['Attack','Meet','Transport','Transfer-Ownership','Start-Position']:
                    check = False
                    for pred in lab_pred_chunks:
                        if pred[0] == ori[0]:
                            print('-->check fault: ', ori, '--', pred)
                            print('   ', lab_chunks)
                            print('   ', lab_pred_chunks)
                            trigger_word = ""
                            for i in range(ori[0], ori[1]):
                                trigger_word += words[i] + ' '
                            print('   trigge word: ', trigger_word)

                            full_sentence = " ".join(word for word in words)
                            print('   full sentence: ', full_sentence)
                            check = True
                            break
                    if not check:
                        print('-->check fault: ', ori, '--')
                        print('   ', lab_chunks)
                        print('   ', lab_pred_chunks)
                        trigger_word = ""
                        for i in range(ori[0], ori[1]):
                            trigger_word += words[i] + ' '
                        print('   Trigger word: ', trigger_word)

                        full_sentence = " ".join(word for word in words)
                        print('   full sentence: ', full_sentence)

    # print('trigger true: ')
    # for trigger in triggers_true:
    #     print(ori_sents[trigger[0]][trigger[1]], trigger[2])
    trigger_p, trigger_r, trigger_f1, num_gold, num_proposed, num_correct = calc_metric(triggers_true, triggers_pred)

    triggers_true = [(item[0], item[1]) for item in triggers_true]
    triggers_pred = [(item[0], item[1]) for item in triggers_pred]
    trigger_p_, trigger_r_, trigger_f1_, _, _, _ = calc_metric(triggers_true, triggers_pred)

    metric = '[trigger classification]\tP={:.3f}\tR={:.3f}\tF1={:.3f}\n'.format(trigger_p, trigger_r, trigger_f1)
    metric += '[trigger identification]\tP={:.3f}\tR={:.3f}\tF1={:.3f}\n'.format(trigger_p_, trigger_r_, trigger_f1_)

    print(collections.Counter(totals).most_common())
    print('\tresult trigger: {}-{}-{}'.format(num_gold, num_proposed, num_correct))

    if check_result is None:
        with open('results/{}_all_score_log.txt'.format(config.id_model), 'a') as f:
            f.write(metric + '\n')
    if check_result is not None:
        print('[trigger classification]')
        print('P={:.3f}\tR={:.3f}\tF1={:.3f}'.format(trigger_p, trigger_r, trigger_f1))

        print('[trigger identification]')
        print('P={:.3f}\tR={:.3f}\tF1={:.3f}'.format(trigger_p_, trigger_r_, trigger_f1_))

        print(collections.Counter(ground_trues).most_common())

    argument_p = 0
    argument_r = 0
    argument_f1 = 0

    return trigger_p * 100, trigger_r * 100, trigger_f1 * 100, argument_p * 100, argument_r * 100, argument_f1 * 100, metric


# @title evaluate func
def evaluate(config: BaseConfig, eval_dataset, model, tokenizer, prefix="", check=None):
    # Note that DistributedSampler samples randomly

    sampler = SequentialSampler(eval_dataset)
    eval_dataloader = DataLoader(eval_dataset,
                                 sampler=sampler,
                                 batch_size=config.eval_batch_size,
                                 collate_fn=pad)
    print("***** Running evaluation {} *****".format(prefix))
    eval_loss = 0.0
    nb_eval_steps = 0
    preds = None
    out_label_ids = None
    ori_sent_ids = None
    model.eval()
    if check is not None:
        eval_dataloader = notebook.tqdm(eval_dataloader)

    trigger_target, trigger_predict, argument_target, argument_predict, ori_sents = [], [], [], [], []
    for batch in eval_dataloader:
        batch = tuple(
            torch.LongTensor(np.array(batch[t])).to(config.device) if t not in [8, 9, 10, 11, 14, 15, 16] else batch[t]
            for t in range(len(batch)))
        with torch.no_grad():
            inputs = {
                "input_ids": batch[0],
                "attention_mask": batch[1],
                "token_type_ids": batch[2],
                "scatter_offsets": batch[3],
                "sparse_offsets": None,
                "input_word_masks": batch[4],
                "character_ids": batch[5],
                "entity_ids": batch[6],
                "edge_ids": batch[7],
                "node_node_adj": batch[8],
                "node_edge_map": batch[9],
                "edge_node_map": batch[10],
                "edge_edge_adj": batch[11],
                "trigger_labels": batch[12],
                "anchor_labels": batch[13],
                "arg_labels": batch[14],
            }

            outputs = model(**inputs)
            tmp_eval_loss = outputs[0]
            eval_loss += tmp_eval_loss.item()
            trigger_predict.extend(outputs[-2].cpu().tolist())
            trigger_target.extend(batch[12].cpu().tolist())
            ori_sents.extend(batch[16])

        nb_eval_steps += 1
        break
    eval_loss = eval_loss / nb_eval_steps

    prec, recall, f1, prec_arg, recall_arg, f1_arg, metrics = checkChunk(trigger_target, trigger_predict,
                                                                         argument_target, argument_predict, ori_sents,
                                                                         config, check)
    results = {
        "loss": eval_loss,
        "precision": prec,
        "recall": recall,
        "f1": f1,
        "precision_arg": prec_arg,
        "recall_arg": recall_arg,
        "f1_arg": f1_arg,
    }
    print('\t', results)
    # for key in results.keys():
    #    print("   {} = {:.4f}".format(key, results[key]))

    return results, metrics

