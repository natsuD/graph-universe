# import torch
#
# # a = torch.tensor([[0, 1, 1, 2],
# #                            [1, 0, 2, 1]], dtype=torch.long).to(torch.device('cuda:0'))
# from torch_geometric.data import Data
#
# edge_index = torch.tensor([[0, 1, 1, 2, 2],
#                            [1, 0, 2, 1, 0]], dtype=torch.long)
# x = torch.tensor([[-2], [1], [1]], dtype=torch.float)
#
# data = Data(x=x, edge_index=edge_index)
# print(data)
# print(data.is_directed())
#
#
#
# from torch_geometric.datasets import TUDataset
from torch_geometric.loader import DataLoader
# import torch_geometric
# import networkx as nx
# import torch
#
#
# import torch
# import torch.nn.functional as F
from torch_geometric.nn import GCNConv, GATConv
#
# from torch_geometric.datasets import Planetoid
#
# dataset = Planetoid(root='/tmp/Cora', name='Cora')
# print(dataset)
#
# class GCN(torch.nn.Module):
#     def __init__(self):
#         super().__init__()
#         self.conv1 = GATConv(dataset.num_node_features, 16)
#         self.conv2 = GATConv(16, dataset.num_classes)
#
#     def forward(self, data):
#         x, edge_index = data.x, data.edge_index
#
#         x = self.conv1(x, edge_index)
#         x = F.relu(x)
#         x = F.dropout(x, training=self.training)
#         x = self.conv2(x, edge_index)
#
#         return F.log_softmax(x, dim=1)
#
#
#
# device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# device
#
#
# device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# model = GCN().to(device)
# data = dataset[0].to(device)
# optimizer = torch.optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-4)
#
# model.train()
# for epoch in range(200):
#     optimizer.zero_grad()
#     out, (edge_index, alpha) = model(data)
#     loss = F.nll_loss(out[data.train_mask], data.y[data.train_mask])
#     loss.backward()
#     optimizer.step()
#
#
# from torch_geometric.loader import DataLoader


import graph_tools

g = graph_tools.Graph(directed=True)

g.add_vertices(*[1,2,3,4,5, 6])
g.add_edge(1,2)
g.add_edge(1,3)
g.add_edge(2,2)
g.add_edge(2,3)
g.add_edge(3,3)
g.add_edge(3,4)
g.add_edge(2,5)
g.add_edge(5, 6)
print(g.shortest_paths(1,2))
g.dijkstra_all_pairs()

print(g)
