import torch

from models.cnn.template_import import *
from models.cnn.embedder import TokenEmbedding
from config.config import BaseConfig
from models.cnn.graph_edge import GraphEdgeNode
from models.cnn.edge import EdgeClassifier


@torch.no_grad()
def generate_anchor_mask(anchor: torch.Tensor):
    anchor = torch.argmax(anchor, dim=-1, keepdim=False)
    batch_size, max_length = anchor.shape
    zeros = torch.zeros(batch_size, max_length, device=anchor.device)
    ones = torch.ones(batch_size, max_length, device=anchor.device)

    result = torch.where(anchor == 1, zeros, ones)

    return result


class EventModel(nn.Module):

    def __init__(self,
                 model_config: BaseConfig,
                 *args,
                 **kwargs):
        super(EventModel, self).__init__()
        self.model_config = model_config
        self.word_embedding = TokenEmbedding(model_config,
                                             *args,
                                             **kwargs)

        self.edge_embedding = nn.Embedding(model_config.edge_vocab_size, model_config.edge_embed_dim)

        self.graph_net = GraphEdgeNode(model_config,
                                       in_node_dim=model_config.out_dim + model_config.entity_embed_dim + model_config.out_dim,
                                       in_edge_dim=model_config.edge_embed_dim,
                                       node_hidden_dim=model_config.hidden_dim,
                                       edge_hidden_dim=model_config.hidden_dim,
                                       num_heads=model_config.num_heads,
                                       dropout=model_config.dropout,
                                       out_dim=model_config.out_dim,
                                       attn_dim=model_config.attn_dim)

        self.anchor_classifier = nn.Sequential(
            nn.Linear(model_config.out_dim * 4, 2),
            nn.ReLU()
        )

        self.edge_net = EdgeClassifier(model_config,
                                       model_config.out_dim * 4,
                                       model_config.out_dim * 4,
                                       model_config.hidden_dim,
                                       presence=False,
                                       label=True)

        self.layerNorm = nn.LayerNorm(model_config.out_dim, eps=1e-8)

        self.fc_trigger = nn.Sequential(
            nn.Dropout(model_config.dropout),
            nn.Linear(
                model_config.out_dim * 4 + model_config.entity_embed_dim + model_config.out_dim * 2, # model_config.out_dim, #
                model_config.trigger_vocab_size)
        )

        self.event_loss = nn.CrossEntropyLoss()
        self.arg_loss = nn.CrossEntropyLoss()
        self.anchor_loss = nn.CrossEntropyLoss()


    def forward(self,
                input_ids: torch.Tensor,
                attention_mask: Optional[torch.Tensor] = None,
                token_type_ids: Optional[torch.Tensor] = None,
                scatter_offsets: Optional[torch.Tensor] = None,
                sparse_offsets=None,
                input_word_masks=None,
                character_ids: torch.Tensor = None,
                entity_ids: torch.Tensor = None,
                edge_ids=None,
                node_node_adj=None,
                node_edge_map=None,
                edge_node_map=None,
                edge_edge_adj=None,
                trigger_labels=None,
                arg_labels=None,
                anchor_labels=None,
                **kwargs
                ):
        # word_ber_dim + char_embed_dim + entity_embed_dim
        device = input_ids.device
        batch_size = input_ids.size(0)
        seq_max_len = character_ids.size(1)
        edge_max_len = edge_ids.size(1)

        word_embedding = self.word_embedding(input_ids,
                                             attention_mask,
                                             token_type_ids,
                                             scatter_offsets,
                                             sparse_offsets,
                                             character_ids,
                                             entity_ids,
                                             **kwargs)

        # edge_embed_dim
        edge_embedding = self.edge_embedding(edge_ids)  # num_edge, hedge

        # convert node node
        node_node_adj = torch.vstack([torch.sparse_coo_tensor(indices=adj['indices'],
                                                              values=adj['values'],
                                                              size=(seq_max_len,
                                                                    seq_max_len)
                                                              ).unsqueeze(0)
                                      for adj in node_node_adj]).to(device)

        # convert edge edge
        edge_edge_adj = torch.vstack([torch.sparse_coo_tensor(indices=adj['indices'],
                                                              values=adj['values'],
                                                              size=(edge_max_len,
                                                                    edge_max_len)
                                                              ).unsqueeze(0)
                                      for adj in edge_edge_adj]).to(device)

        # convert node_edge
        node_edge_map = torch.vstack([torch.sparse_coo_tensor(indices=adj['indices'],
                                                              values=adj['values'],
                                                              size=(seq_max_len,
                                                                    seq_max_len)
                                                              ).unsqueeze(0)
                                      for adj in node_edge_map]).to(device)
        ori_size = node_edge_map.size()
        node_edge_adj_flatten = node_edge_map.to_dense().contiguous().view(batch_size, -1).unsqueeze(-1)
        new_shape = list(node_edge_adj_flatten.size()[:-1]) + [self.model_config.edge_embed_dim, ]
        node_edge_adj_flatten = node_edge_adj_flatten.expand(*new_shape)
        leave_first = torch.cat((torch.zeros(edge_embedding.size(0), 1, edge_embedding.size(2), requires_grad=False).to(device),
                                 edge_embedding),
                                dim=1) # leave first element, map id start from 1
        node_edge_embedding = torch.gather(leave_first, dim=1, index=node_edge_adj_flatten).contiguous().view(ori_size + (edge_embedding.size(-1),))
        node_edge_adj = torch.sparse_coo_tensor(indices=node_edge_map.coalesce().indices(),
                                                values=[1] * len(node_edge_map.coalesce().indices()[0]), size=ori_size)

        # convert edge_node
        edge_node_map = torch.vstack([torch.sparse_coo_tensor(indices=adj['indices'],
                                                              values=adj['values'],
                                                              size=(edge_max_len,
                                                                    edge_max_len)
                                                              ).unsqueeze(0)
                                      for adj in edge_node_map])
        ori_size = edge_node_map.size()
        edge_node_adj_flatten = edge_node_map.to_dense().contiguous().view(batch_size, -1).unsqueeze(-1)
        new_shape = list(edge_node_adj_flatten.size()[:-1]) + [word_embedding.size(-1), ]
        edge_node_adj_flatten = edge_node_adj_flatten.expand(*new_shape)
        leave_first = torch.cat((torch.zeros(word_embedding.size(0), 1, word_embedding.size(2), requires_grad=False).to(device),
                                 word_embedding),
                                dim=1) # leave first element, map id start from 1

        edge_node_embedding = torch.gather(leave_first, dim=1, index=edge_node_adj_flatten).view(ori_size + (word_embedding.size(-1),))
        edge_node_adj = torch.sparse_coo_tensor(indices=edge_node_map.coalesce().indices(),
                                                values=[1] * len(edge_node_map.coalesce().indices()[0]), size=ori_size)

        node_embed, edge_embed = self.graph_net(word_embedding, edge_embedding,
                                                edge_node_embedding, node_edge_embedding,
                                                node_node_adj, node_edge_adj,
                                                edge_node_adj, edge_edge_adj)

        anchor_logits = self.anchor_classifier(node_embed)
        anchor_mask = generate_anchor_mask(anchor_logits)

        # merge biaffine edge classify with dependency embed
        ori_size = node_edge_map.size()
        node_edge_adj_flatten = node_edge_map.to_dense().contiguous().view(batch_size, -1).unsqueeze(-1)
        new_shape = list(node_edge_adj_flatten.size()[:-1]) + [edge_embed.size(-1),]
        node_edge_adj_flatten = node_edge_adj_flatten.expand(*new_shape)
        leave_first = torch.cat((torch.zeros(edge_embed.size(0), 1, edge_embed.size(2), requires_grad=False, device=device),
                                 edge_embed),
                                dim=1)  # leave first element, map id start from 1

        neighbor_edge_embedding = torch.gather(leave_first, dim=1, index=node_edge_adj_flatten).contiguous().view(
            ori_size + (edge_embed.size(-1),))

        _, edge_logits = self.edge_net(node_embed, neighbor_edge_embedding)

        trigger_logits = self.fc_trigger(torch.cat([node_embed, word_embedding], dim=-1))

        loss = None
        if trigger_labels is not None:
            active_loss = input_word_masks.view(-1) == 1
            active_logits = trigger_logits.view(-1, self.model_config.trigger_vocab_size)[active_loss]
            active_labels = trigger_labels.view(-1)[active_loss]
            event_loss = self.event_loss(active_logits, active_labels)
            loss = event_loss * 1

        if arg_labels is not None:
            # edge_labels [B, seq, seq, ntype] -> flatten
            anchor_mask = anchor_mask.unsqueeze(-1).repeat_interleave(repeats=seq_max_len, dim=-1).view(-1) == 1
            arg_mask = torch.vstack([F.pad(torch.ones(sent_len, sent_len), (0, seq_max_len - sent_len, 0, seq_max_len - sent_len).to(device), value=0)
                                     for sent_len in input_word_masks.sum(dim=-1)])
            active_loss = arg_mask.view(-1) == 1
            active_loss = torch.logical_and(active_loss, anchor_mask)  # only backward with not mask and anchor token
            active_logits = edge_logits.view(-1, self.model_config.argument_vocab_size)[active_loss]
            dense_arg_labels = torch.vstack([torch.sparse_coo_tensor(indices=adj['indices'],
                                                              values=adj['values'],
                                                              size=(seq_max_len,
                                                                    seq_max_len),
                                                              dtype=torch.long).unsqueeze(0)
                                      for adj in arg_labels]).to_dense().to(device)
            active_labels = dense_arg_labels.view(-1)[active_loss]
            arg_loss = self.arg_loss(active_logits, active_labels)
            loss += arg_loss * 0.5
            pass

        if anchor_labels is not None:
            active_loss = input_word_masks.view(-1) == 1
            active_logits = anchor_logits.view(-1, 2)[active_loss]
            active_labels = anchor_labels.view(-1)[active_loss]
            anchor_loss = self.anchor_loss(active_logits, active_labels)
            loss += 0.5 * anchor_loss
            pass

        output = (loss, trigger_logits, edge_logits, anchor_logits, F.softmax(trigger_logits, dim=-1).argmax(dim=-1), F.softmax(edge_logits, dim=-1).argmax(dim=-1))
        # output = (loss,trigger_logits)
        return output

        pass


