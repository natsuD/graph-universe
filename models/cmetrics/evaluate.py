import torch
from torch.utils.data import SequentialSampler, DataLoader
from tqdm import notebook

from config.config import BaseConfig

CLS = "CLS"
SEP = "SEP"

def find_triggers(labels, ori_words, args):
    """

    :param labels: ['B-Conflict:Attack', 'I-Conflict:Attack', 'O', 'B-Life:Marry']
    :param input_ids: seq_len, [cls_id, 12,2,4,1,..., sep_id, pad_id,...]
    :return: [(0, 2, 'Conflict:Attack'), (3, 4, 'Life:Marry')]
    """
    segs = {}
    type_trigger = ""
    st, ed = -1, -1
    for i, (x, wtoken) in enumerate(zip(labels, ori_words)):
        if wtoken == CLS:
            continue
        if wtoken == SEP:
            break
        # refine label predict for subword
        if wtoken.startswith('##'):
            temp_idx = i - 1
            while (ori_words[temp_idx].startswith('##') and temp_idx >= 1):
                temp_idx -= 1
            if labels[temp_idx] != 'O':
                labels[i] = 'I-' + labels[temp_idx][2:]
            else:
                labels[i] = 'O'

    for i, (x, wtoken) in enumerate(zip(labels, ori_words)):
        if wtoken == CLS:
            continue
        if wtoken == SEP:
            break

        if x.startswith("B-"): # start with B-
            if type_trigger == "":
                type_trigger = x[2:]
                st = i
            else:  # take new B-
                ed = i
                segs[st] = (ed, type_trigger)
                type_trigger = x[2:]
                st = i
        elif x.startswith("I-"):
            if type_trigger == "": # start group with I- -> convert to B-
                labels[i] = "B" + labels[i][1:]
                type_trigger = x[2:]
                st = i
            else:
                if type_trigger != x[2:]:  # I- of difference trigger type with prior trigger token
                    ed = i
                    segs[st] = (ed, type_trigger)
                    labels[i] = "B" + labels[i][1:]
                    type_trigger = x[2:]
                    st = i
        else:
            ed = i  # take O, end current group trigger
            if type_trigger != "":
                segs[st] = (ed, type_trigger)
            type_trigger = ""

    if type_trigger != "":
        segs[st] = (i, type_trigger)

    results = []
    for st in segs:
        results.append((st, *segs[st]))

    return results

def calc_metric(y_true, y_pred):
    """
    :param y_true: [(tuple), ...]
    :param y_pred: [(tuple), ...]
    :return:
    """
    num_proposed = len(y_pred)
    num_gold = len(y_true)

    y_true_set = set(y_true)
    num_correct = 0
    for item in y_pred:
        if item in y_true_set:
            num_correct += 1

    print('proposed: {}\tcorrect: {}\tgold: {}'.format(num_proposed, num_correct, num_gold))

    if num_proposed != 0:
        precision = num_correct / num_proposed
    else:
        precision = 1.0

    if num_gold != 0:
        recall = num_correct / num_gold
    else:
        recall = 1.0

    if precision + recall != 0:
        f1 = 2 * precision * recall / (precision + recall)
    else:
        f1 = 0

    return precision, recall, f1, num_gold, num_proposed, num_correct


def checkChunk(argument_target, argument_predict, ori_sents, args, check_result=None):
    # compare trigger_target set with trigger_predict set have printing all missing label

    id2event = dict(zip(args.vocab_event.values(), args.vocab_event.keys()))
    correct_preds, total_correct, total_preds = 0., 0., 0.
    totals = []
    ground_trues = []

    triggers_pred, triggers_true, arguments_true, arguments_pred = [], [], [], []
    if check_result is None:
        fout = open('results/log_predict_{}.txt'.format(args.id_model), 'w')
    else:
        print('argument: ', argument_predict)
    for ids, (words, arg_predict, arg_target) in enumerate(zip(ori_sents, argument_predict, argument_target)):

        # argument proces
        for trigger in arg_target['events']:
            t_start, t_end, t_type_str = trigger
            for argument in arg_target['events'][trigger]:
                a_start, a_end, a_type_idx = argument
                arguments_true.append((ids, t_start, t_end, t_type_str, a_start, a_end, a_type_idx))

        # for trigger in arg_target['events']:
        #     t_start, t_end, t_type_str = trigger
        #     if (ids, t_start, t_type_str) in triggers_predict:
        #         for argument in arg_target['events'][trigger]:
        #             a_start, a_end, a_type_idx = argument
        #             arguments_true.append((ids, t_start, t_end, t_type_str, a_start, a_end, a_type_idx))

        for trigger in arg_predict:
            t_start, t_end, t_type_str = trigger
            for argument in arg_predict[trigger]:
                a_start, a_end, a_type_idx = argument
                arguments_pred.append((ids, t_start, t_end, t_type_str, a_start, a_end, a_type_idx))

        # for trigger in arg_predict:
        #     t_start, t_end, t_type_str = trigger
        #     trigger_check_in = False
        #     for trigger_targ in arg_target['events']:
        #         if trigger == trigger_targ:
        #             trigger_check_in = True

        #     if trigger_check_in:
        #         for argument in arg_predict[trigger]:
        #             a_start, a_end, a_type_idx = argument
        #             arguments_pred.append((ids, t_start, t_end, t_type_str, a_start, a_end, a_type_idx))

        if check_result is None:
            for idw, w in enumerate(zip(words)):
                fout.write('{}\t{}\n'.format(idw, w))
            fout.write('#arguments#{}\n'.format(arg_target['events']))
            fout.write('#arguments_hat#{}\n'.format(arg_predict))
            fout.write("\n\n")

    argument_p, argument_r, argument_f1, num_gold_arg, num_proposed_arg, num_correct_arg = calc_metric(arguments_true,
                                                                                                       arguments_pred)

    arguments_true = [(item[0], item[1], item[2], item[3], item[4], item[5]) for item in arguments_true]
    arguments_pred = [(item[0], item[1], item[2], item[3], item[4], item[5]) for item in arguments_pred]
    argument_p_, argument_r_, argument_f1_, _, _, _ = calc_metric(arguments_true, arguments_pred)

    metric = '[argument classification]\tP={:.3f}\tR={:.3f}\tF1={:.3f}\n'.format(argument_p, argument_r, argument_f1)
    metric += '[argument identification]\tP={:.3f}\tR={:.3f}\tF1={:.3f}\n'.format(argument_p_, argument_r_,
                                                                                  argument_f1_)
    print('\t       argmuent: {}-{}-{}'.format(num_gold_arg, num_proposed_arg, num_correct_arg))

    if check_result is None:
        with open('results/{}_all_score_log.txt'.format(args.id_model), 'a') as f:
            f.write(metric + '\n')
    if check_result is not None:
        print('[argument classification]')
        print('P={:.3f}\tR={:.3f}\tF1={:.3f}'.format(argument_p, argument_r, argument_f1))

        print('[argument identification]')
        print('P={:.3f}\tR={:.3f}\tF1={:.3f}'.format(argument_p_, argument_r_, argument_f1_))

    return argument_p * 100, argument_r * 100, argument_f1 * 100, metric


# @title evaluate func
def evaluate(config: BaseConfig, eval_dataset, model, tokenizer, prefix="", check=None):
    # Note that DistributedSampler samples randomly

    sampler = SequentialSampler(eval_dataset)
    eval_dataloader = DataLoader(eval_dataset,
                                 sampler=sampler,
                                 batch_size=config.test_batch_size,
                                 collate_fn=pad)
    print("***** Running evaluation {} *****".format(prefix))
    eval_loss = 0.0
    nb_eval_steps = 0
    preds = None
    out_label_ids = None
    ori_sent_ids = None
    count = 0
    model.eval()
    if check is not None:
        eval_dataloader = notebook.tqdm(eval_dataloader)

    trigger_target, trigger_predict, argument_target, argument_predict, ori_sents = [], [], [], [], []
    for batch in eval_dataloader:
        batch = tuple(
            torch.LongTensor(batch[t]).to(config.device) if t in [0, 1, 2, 4] else batch[t] for t in range(len(batch)))
        with torch.no_grad():
            inputs = {"input_ids": batch[0],
                      "attention_mask": batch[1],
                      "token_type_ids": batch[2],
                      "input_ners": batch[3],
                      "input_postags": batch[4],
                      "arguments": batch[5],
                      "ori_words": batch[6],
                      "labels": batch[7]}

            outputs = model(**inputs)
            tmp_eval_loss = outputs[0]
            if tmp_eval_loss is not None:
                eval_loss += tmp_eval_loss.item()
                count += 1
            argument_predict.extend(outputs[-2])
            argument_target.extend(batch[5])
            ori_sents.extend(batch[6])

        nb_eval_steps += 1
    eval_loss = eval_loss / count

    prec_arg, recall_arg, f1_arg, metrics = checkChunk(argument_target, argument_predict, ori_sents, config, check)
    results = {
        "loss": eval_loss,
        "precision": 0,
        "recall": 0,
        "f1": 0,
        "precision_arg": prec_arg,
        "recall_arg": recall_arg,
        "f1_arg": f1_arg,
    }
    print('\t', results)
    # for key in results.keys():
    #    print("   {} = {:.4f}".format(key, results[key]))

    return results, metrics

def pad(batch):
    return list(map(list, zip(*batch)))