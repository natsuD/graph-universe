from .template_import import *
class CharacterEmbedding(nn.Module):
    """
    embedding + lstm + linear
    """
    def __init__(self,
                embedding_dim: int,
                num_embeddings: int,
                hidden_dim: int,
                output_dim: int,
                dropout: float,
                ):

        super(CharacterEmbedding, self).__init__()
        self.hidden_dim = hidden_dim
        self.char_embed_matrix = nn.Embedding(num_embeddings=num_embeddings, embedding_dim=embedding_dim,
                                         padding_idx=0, max_norm=3., norm_type=2)

        self.lstm = nn.GRU(input_size=embedding_dim, hidden_size=hidden_dim // 2, batch_first=True, dropout=dropout, bidirectional=True)
        # self.attention = nn.MultiheadAttention(embed_dim=hidden_size, num_heads=3)
        self.linear = nn.Linear(in_features=hidden_dim, out_features=output_dim)

    def forward(self, char_inputs: torch.Tensor):
        """

        :param char_embed:
        :return: [batch_size, seq_len, char_embed]
        """

        # [batch, seq_len, word_len] -> [batch * seq_len, word_len]
        batch_size = char_inputs.size(dim=0)
        word_max_len = char_inputs.size(2)
        seq_max_len = char_inputs.size(1)
        char_reshape_input = char_inputs.contiguous().view(-1, word_max_len)
        char_embedding = self.char_embed_matrix(char_reshape_input)
        _, lstm_final_hidden = self.lstm(char_embedding) # lstm output, [direction * num_layer, batch_size, hidden] -> [batch_size, direction * n_layer, hidden]
        lstm_out = torch.transpose(lstm_final_hidden, 0, 1).contiguous().view(-1, self.hidden_dim)
        final_embed = self.linear(lstm_out)

        return final_embed.contiguous().view(batch_size, seq_max_len, -1)

