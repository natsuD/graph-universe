#!/usr/bin/env python3
# coding=utf-8

import torch
import torch.nn as nn
import torch.nn.functional as F

from config.config import BaseConfig
from models.cnn.biaffine import Biaffine


class EdgeClassifier(nn.Module):
    def __init__(self, model_config: BaseConfig,
                 input_dim,
                 dep_dim,
                 hidden_dim,
                 presence: bool = False,
                 label: bool=True):

        super(EdgeClassifier, self).__init__()

        self.presence = presence
        if self.presence:
            self.edge_presence = EdgeBiaffine(input_dim, dep_dim, hidden_dim // 2, 1, model_config.dropout)

        self.label = label
        if self.label:
            self.edge_label = EdgeBiaffine(input_dim, dep_dim, hidden_dim, model_config.argument_vocab_size, model_config.dropout)

    def forward(self, x, dep_embed):
        presence, label = None, None

        if self.presence:
            presence = self.edge_presence(x).squeeze(-1)  # shape: (B, T, T)
        if self.label:
            label = self.edge_label(x, dep_embed)  # shape: (B, T, T, O_1)

        return presence, label


class EdgeBiaffine(nn.Module):
    def __init__(self, hidden_dim, dep_dim, bottleneck_dim, output_dim, dropout):
        super(EdgeBiaffine, self).__init__()
        self.hidden = nn.Linear(hidden_dim, 2 * bottleneck_dim)
        self.output = Biaffine(bottleneck_dim, bottleneck_dim)
        self.classifier = nn.Linear(dep_dim + bottleneck_dim, output_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x, dep_edge):
        x = self.dropout(F.elu(self.hidden(x)))  # shape: (B, T, 2H)
        predecessors, current = x.chunk(2, dim=-1)  # shape: (B, T, H), (B, T, H)
        edge = self.output(current, predecessors)  # shape: (B, T, T, O)
        edge = self.classifier(torch.cat((edge, dep_edge), dim=-1))
        return edge
