import math

import torch

from models.cnn.template_import import *
from models.cnn.embedder import TokenEmbedding
from config.config import BaseConfig


class GraphEdgeNode(nn.Module):
    def __init__(self,
                model_config: BaseConfig,
                in_node_dim=10,
                in_edge_dim = 10,
                node_hidden_dim = 10,
                edge_hidden_dim = 10,
                num_heads = 2,
                dropout = 0.,
                out_dim = 20,
                attn_dim = 30):

        super(GraphEdgeNode, self).__init__()

        self.in_node_dim = in_node_dim
        self.in_edge_dim = in_edge_dim
        self.node_hidden_dim = node_hidden_dim
        self.edge_hidden_dim = edge_hidden_dim
        self.num_heads = num_heads
        self.dropout = nn.Dropout(dropout)


        self.node_node_attn_layer = MultiHeadGraphAttention(in_node_dim, in_node_dim, node_hidden_dim,
                                                            node_hidden_dim, out_dim, attn_dim, num_heads, dropout)
        if in_edge_dim is not None:
            self.node_edge_attn_layer = MultiHeadGraphAttention(in_node_dim, in_edge_dim, node_hidden_dim,
                                                                edge_hidden_dim, out_dim, attn_dim, num_heads, dropout)

            self.edge_node_attn_layer = MultiHeadGraphAttention(in_edge_dim, in_node_dim, edge_hidden_dim,
                                                                node_hidden_dim, out_dim, attn_dim, num_heads, dropout)

            self.edge_edge_attn_layer = MultiHeadGraphAttention(in_edge_dim, in_edge_dim, edge_hidden_dim,
                                                                edge_hidden_dim, out_dim, attn_dim, num_heads, dropout)

        pass


    def forward(self, node_feat, edge_feat=None, node_key_feat=None, edge_key_feat=None,
                node_node_adj=None, node_edge_adj=None,
                edge_node_adj=None, edge_edge_adj=None):

        if node_key_feat is None:
            node_key_feat = node_feat

        if edge_key_feat is None:
            edge_key_feat = edge_feat

        list_out_node = []
        list_out_edge = []
        node_base_attn_node, _ = self.node_node_attn_layer(node_feat, node_feat, node_node_adj)
        list_out_node.append(node_base_attn_node)
        if self.in_edge_dim:
            edge_base_attn_node, _ = self.node_edge_attn_layer(node_feat, edge_key_feat, node_edge_adj)

            list_out_node.append(edge_base_attn_node)
            node_base_attn_edge, _ = self.edge_node_attn_layer(edge_feat, node_key_feat, edge_node_adj)

            list_out_edge.append(node_base_attn_edge)

            edge_base_attn_edge, _ = self.edge_edge_attn_layer(edge_feat, edge_feat, edge_edge_adj)
            list_out_edge.append(edge_base_attn_edge)

        if len(list_out_node) > 1:
            # list [B, N, hdim]
            output_feat = torch.cat(list_out_node, dim=-1)
        else:
            output_feat = list_out_node[0]

        if len(list_out_edge) > 1:
            output_feat_edge = torch.cat(list_out_edge, dim=-1)
        elif len(list_out_edge) == 1:
            output_feat_edge = list_out_edge[0]

        if len(list_out_edge) > 0:
            return output_feat, output_feat_edge

        return output_feat


        pass


class MultiHeadGraphAttention(nn.Module):
    def __init__(self,
                 in_query_dim: int,
                 in_key_dim: Optional[int] = None,
                 query_hidden_dim: int = 100,
                 key_hidden_dim: Optional[int] = None,
                 out_dim: int = 100,
                 attn_dim = 100,
                 num_heads: int = 1,
                 dropout: float = 0.,
                 ):

        super(MultiHeadGraphAttention, self).__init__()

        self.num_heads = num_heads
        self.attn_dim = attn_dim
        self.out_dim = out_dim
        # self.linear_query = nn.Linear(in_features=in_query_dim, out_features=num_heads * query_hidden_dim)
        # self.linear_key = nn.Linear(in_features=in_key_dim, out_features=num_heads * key_hidden_dim)
        #

        self.attn_query = nn.Linear(in_features=in_query_dim, out_features=num_heads * attn_dim)
        self.attn_key = nn.Linear(in_features=in_key_dim, out_features=num_heads * attn_dim)
        self.attn_value = nn.Linear(in_features=in_key_dim, out_features=num_heads * out_dim)

        self.reset_parameters()
    def reset_parameters(self):
        # self.linear_query.reset_parameters()
        # self.linear_key.reset_parameters()

        self.attn_query.reset_parameters()
        self.attn_key.reset_parameters()


    def forward(self,
                query_feat,
                key_feat,
                adj,
                return_attn=True,
                reduce_method="concate"):
        """

        :param query_feat: [B, Seq1, h1]
        :param key_feat:  [B,Seq2, h2] or [B, Seq2, Seq2, h3]
        :param adj:
        :param return_attn:
        :return:
        """
        device = query_feat.device
        seq_query_len = query_feat.size(1)
        seq_key_len = key_feat.size(1)

        query_attn = self.attn_query(query_feat)
        key_attn = self.attn_key(key_feat)
        value_attn = self.attn_value(key_feat)

        # reshape
        query_attn = self.reshape_key(query_attn, out_dim=self.attn_dim, inp_type="query")
        key_attn = self.reshape_key(key_attn, out_dim=self.attn_dim, inp_type="key", seq_ref_size=seq_query_len)
        value_attn = self.reshape_key(value_attn, out_dim=self.out_dim, inp_type="value", seq_ref_size=seq_query_len)


        attn_score = torch.matmul(query_attn, key_attn.transpose(-1, -2)).squeeze(-2) # [B,H,seq,1,h1'] x [B,H,seq,seq2,h1'] -> [B,H,seq,1,seq2] -> [B,H,seq,seq2]
        attn_score_norm = attn_score / math.sqrt(self.num_heads)
        # attn_score_norm = attn_score_norm.contiguous().view(-1, self.num_heads, seq_query_len, seq_key_len)

        zero_vec = -9e15 * torch.ones_like(attn_score_norm).to(device)
        attn_score_norm = torch.where(adj.to_dense().unsqueeze(1) > 0,
                                      attn_score_norm,
                                      zero_vec)
        attn_prob = F.softmax(attn_score_norm, dim=-1).unsqueeze(-2)

        # [B, H, Seq, 1, Seq2] x [B, H, Seq, Seq2, hdim] -> [B, H, Seq, 1, hdim] -> [B,H,Seq, hdim]
        out_feat = torch.matmul(attn_prob, value_attn).squeeze(-2)
        out_feat = out_feat.transpose(1, 2)
        if reduce_method == "concate":
            out_feat = out_feat.contiguous().view(list(out_feat.size())[:2] + [-1])  # [B, N, hdim * H]
        elif reduce_method == "mean":
            out_feat = torch.mean(out_feat, dim=-2)

        if return_attn:
            return out_feat, attn_prob
        else:
            return out_feat

        pass


    def reshape_key(self, input, out_dim, inp_type="query", seq_ref_size=None):
        # print(x.shape)
        if inp_type == "query":
            input = input.unsqueeze(-2)
            new_shape = input.size()[:-1] + (self.num_heads, out_dim)
            input = input.contiguous().view(*new_shape)  # [B, Seq, 1, H, h1]
            input = input.permute(0, 3, 1, 2, 4)  # [B, H, Seq, 1, h1]
            return input
        else:
            if len(input.shape) == 3: # [B, Seq, h] -> [B, 1, seq, h] -> [B, seq_ref, seq, h]
                assert seq_ref_size is not None, "Seq_ref_size is not None for 3 dim input"
                input = torch.repeat_interleave(input.unsqueeze(1), repeats=seq_ref_size, dim=1)
                new_x_shape = input.size()[:-1] + (self.num_heads, out_dim)

                input = input.contiguous().view(*new_x_shape) #  -> [B, seq_ref, seq, H, h']
            else: # 4  [B, seq_ref, Seq, h]
                new_x_shape = input.size()[:-1] + (self.num_heads, out_dim)
                input = input.contiguous().view(*new_x_shape) # -> [B, seq_ref, Seq, H, h']

            return input.permute(0, 3, 1, 2, 4) # -permute-> [B, H, seq_ref, seq, h']


if __name__ == '__main__':
    model = GraphEdgeNode()

    model.train()
    for _ in range(5):
        # out = model(node_feat=torch.randn((2, 4, 3)),
        #                     edge_feat=torch.randn((2, 7, 3)),
        #                     node_node_adj=torch.randint(0, 2, (2, 4,4)),
        #                     node_edge_map=torch.randint(0, 2, (2, 4, 7)),
        #                     edge_node_map=torch.randint(0, 2, (2, 7, 4)),
        #                     edge_edge_adj=torch.randint(0, 2, (2, 7, 7)))

        out = model(node_feat=torch.randn((2, 4, 3)),
                    edge_feat=torch.randn((2, 4, 4, 3)),
                    node_node_adj=torch.randint(0, 2, (2, 4, 4)),
                    node_edge_adj=torch.randint(0, 2, (2, 4, 4)),
                    edge_node_adj=torch.randint(0, 2, (2, 7, 4)),
                    edge_edge_adj=torch.randint(0, 2, (2, 7, 7)))
    print(out)