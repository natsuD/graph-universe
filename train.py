import glob
import os
import pickle

import numpy as np
import torch
from torch.optim import AdamW
from torch.utils.data import WeightedRandomSampler, DataLoader
from torch.utils.tensorboard import SummaryWriter
from tqdm import notebook

from config.config import BaseConfig
from dataset_processing.ace_dataset import ACE_DATASET, pad
from models.cnn.tokenizer import Tokenizer
from models.model import EventModel
from utils.evaluate import evaluate


#@title create optimizer
def create_optimizer(model, config: BaseConfig, full_finetune=True, learning_rate=None):
    no_decay = ["bias", "LayerNorm.weight"]
    bert_params = model.word_embedding.transformer_encoding.transformer_model.named_parameters()
    top_layer_params = []
    for n, p in model.named_parameters():
        if "transformer_model" in n:
            continue
        top_layer_params.append(p)
    if full_finetune:
        opt_grouped_params = [
                    {"params": top_layer_params,
                    "weight_decay": config.weight_decay,
                    },
                    {
                    "params": [p for n, p in  bert_params if not any(nd in n for nd in no_decay)],
                    "weight_decay": config.weight_decay_bert,
                    },
                    {
                    "params": [p for n, p in bert_params if any(nd in n for nd in no_decay)],
                    'weight_decay': 0.0
                    },]
    else:
        opt_grouped_params = [
                    {"params": top_layer_params,
                    "weight_decay": config.weight_decay,
                    }]

    if learning_rate is None:
        learning_rate = config.learning_rate

    optimizer = AdamW(opt_grouped_params, lr=learning_rate, eps=config.adam_epsilon)
    return optimizer


if __name__ == '__main__':

    tokenizer = Tokenizer("./bert-base-cased", return_sparse_offsets=False)

    config = BaseConfig()
    config.weight_decay = 1e-5
    config.weight_decay_bert = 5e-5
    config.learning_rate = 5e-3

    config.logging_steps = 10
    config.train_batch_size = 2
    config.eval_batch_size = 2
    config.warmup_steps = 1500
    config.num_train_epochs = 100

    global_steps = 0.
    f1_best = 48.61
    f1_test = 45.5726
    logging_loss, tr_loss = 0., 0.
    epoch_improve = 0.
    restart_used = 0
    lr_change_step = 1
    model_name = 'modelarg.ckpt'
    model_test_name = 'modetestlarg.ckpt'
    log_name = 'modelarg.txt'
    tensorboard_name = 'tb_modelarg'

    model = EventModel(config)
    model.to(config.device)

    optimizer = create_optimizer(model, config, False)



    try:
        tb_writer.close()
    except:
        pass
    if not os.path.exists(config.save_path):
        os.makedirs(config.save_path)


    # scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=config.warmup_steps,
    #                                             num_training_steps=total_steps)

    check_change_sample_rate = False
    lambda2 = lambda epoch: 1 / (1 + 0.05 * epoch)
    scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda2)

    tb_writer = SummaryWriter(os.path.join(config.save_path, tensorboard_name))

    files = glob.glob(config.reload_path + "*_{}.pkl".format("train"))

    count = 0

    # scheduler.step()
    # torch.autograd.set_detect_anomaly(True)
    for ep in range(int(config.num_train_epochs)):
        for file in files[1]:
            test_dataset = dev_dataset = train_dataset = ACE_DATASET(BaseConfig(),
                                                                     path=file.split("/")[-1],
                                                                     rebuild=False)  # ACE_DATASET(BaseConfig(), typedt="dev", path="sub_dataset_dev.pkl")

            train_sampler = WeightedRandomSampler(*train_dataset.get_samples_weight())
            # train_sampler = RandomSampler(train_dataset)
            train_loader = DataLoader(train_dataset,
                                      sampler=train_sampler,
                                      batch_size=config.train_batch_size,
                                      collate_fn=pad)

            total_steps = int((len(train_loader) / lr_change_step) * config.num_train_epochs) + 1
            config.warmup_steps = total_steps // 7

            print('-> Start training process')
            print('nepoch: ', config.num_train_epochs)
            print('batch size: ', config.train_batch_size)
            print('step per epoch: ', len(train_loader))
            print('warmup step: ', config.warmup_steps)
            print('num times change learning rate: ', total_steps)

            train_iterator = notebook.tqdm(train_loader)
            if not check_change_sample_rate and f1_best > 50:
                train_sampler = WeightedRandomSampler(*train_dataset.get_samples_weight(rate=1))
                train_loader = DataLoader(train_dataset,
                                          sampler=train_sampler,
                                          batch_size=config.train_batch_size,
                                          collate_fn=pad)
                check_change_sample_rate = True
            count_loss = 0
            for step, batch in enumerate(train_iterator):
                global_steps += 1
                model.train()
                model.zero_grad()
                batch = tuple(torch.LongTensor(np.array(batch[t])).to(config.device) if t not in [8,9,10,11,14,15,16] else batch[t]
                              for t in range(len(batch)))
                inputs = {
                    "input_ids": batch[0],
                    "attention_mask": batch[1],
                    "token_type_ids": batch[2],
                    "scatter_offsets": batch[3],
                    "sparse_offsets": None,
                    "input_word_masks": batch[4],
                    "character_ids": batch[5],
                    "entity_ids": batch[6],
                    "edge_ids": batch[7],
                    "node_node_adj": batch[8],
                    "node_edge_map": batch[9],
                    "edge_node_map": batch[10],
                    "edge_edge_adj": batch[11],
                    "trigger_labels": batch[12],
                    "anchor_labels": batch[13],
                    "arg_labels": batch[14],
                }

                outputs = model(**inputs)
                loss = outputs[0]
                if loss is not None:
                    tr_loss += loss.item()
                    count_loss += 1
                    train_iterator.set_description(
                        "Epoch {}/{}(lr = {:.8f})-l={:.3f}".format(int(ep), int(config.num_train_epochs),
                                                                   optimizer.param_groups[0]['lr'], loss.item()))

                    loss.backward()
                    torch.nn.utils.clip_grad_norm_(model.parameters(), 1)
                    optimizer.step()

                # if global_steps % lr_change_step == 0:
                if global_steps % len(train_loader) == 0:
                    scheduler.step()
                    if optimizer.param_groups[0]['lr'] == config.learning_rate:
                        with open(os.path.join(config.save_path, log_name), 'a', encoding='utf-8') as f:
                            f.write('reach top learning rate at EPOCH {}/ STEP {}'.format(ep, global_steps))
                    print('->scheduler run')
                    if optimizer.param_groups[0]['lr'] < 1e-6:
                        print('-->reset optimizer')
                        optimizer = create_optimizer(model, config, learning_rate=config.learning_rate * (
                                    1 - restart_used / (1 + config.max_restart)))
                        scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda2)

                if config.logging_steps > 0 and (global_steps % config.logging_steps == 0 or step == len(train_iterator) - 1):
                    print('lr = {}\n'.format(optimizer.param_groups[0]['lr']))

                    results, _ = evaluate(config, dev_dataset, model, tokenizer,
                                          prefix='dev set, step {}/{}'.format(global_steps, ep))
                    test_results, _ = evaluate(config, test_dataset, model, tokenizer,
                                               prefix='test set, step {}/{}'.format(global_steps, ep))
                    for key, value in results.items():
                        if key != 'loss':
                            tb_writer.add_scalar("{} score".format(key), value, global_steps)
                    tb_writer.add_scalar("learning rate", scheduler.get_last_lr()[0], global_steps)
                    # tb_writer.add_scalars("loss", {'train_loss': (tr_loss - logging_loss) / config.logging_steps,
                    #                                    'dev_loss': results['loss']}, global_steps)
                    tb_writer.add_scalars("loss", {'train_loss': (tr_loss - logging_loss) / count_loss,
                                                   'dev_loss': results['loss']}, global_steps)
                    count_loss = 0
                    logging_loss = tr_loss

                    if results['f1'] + results['f1_arg'] > f1_best:
                        f1_best = results['f1'] + results['f1_arg']
                        epoch_improve = ep
                        print('--> New best score! f1 = ', f1_best)
                        torch.save(model.state_dict(), os.path.join(config.save_path, model_name))
                        with open(os.path.join(config.save_path, log_name), 'a', encoding='utf-8') as f:
                            f.write('Epoch: {:3.0f}, step: {:4.0f} global_step: {:5.0f} (lr= {:.7f})\n\
                                    Results trigger : P= {:.4f} - R= {:.4f} - F= {:.4f} \n\
                                    Results argument: P= {:.4f} - R= {:.4f} - F= {:.4f} \n\t--=>>>New best score!\n'.format(
                                ep, step,
                                global_steps,
                                optimizer.param_groups[0]['lr'],
                                results['precision'],
                                results['recall'],
                                results['f1'],
                                results['precision_arg'],
                                results['recall_arg'],
                                results['f1_arg']))


                    else:
                        with open(os.path.join(config.save_path, log_name), 'a', encoding='utf-8') as f:
                            f.write('Epoch: {:3.0f}, step: {:4.0f} global_step: {:5.0f} (lr= {:.7f})\n\
                                    Dev Results: P= {:.4f} - R= {:.4f} - F= {:.4f}\n\
                                    Results argument: P= {:.4f} - R= {:.4f} - F= {:.4f} \n'.format(ep, step, global_steps,
                                                                                                   optimizer.param_groups[
                                                                                                       0]['lr'],
                                                                                                   results['precision'],
                                                                                                   results['recall'],
                                                                                                   results['f1'],
                                                                                                   results['precision_arg'],
                                                                                                   results['recall_arg'],
                                                                                                   results['f1_arg']))

                        if ep - epoch_improve > 10:
                            if restart_used > config.max_restart or not os.path.exists(
                                    os.path.join(config.save_path, model_name)):
                                break
                            else:
                                restart_used += 1
                                print('--->>>RELOAD MODEL from epoch {}'.format(epoch_improve))
                                with open(os.path.join(config.save_path, log_name), 'a', encoding='utf-8') as f:
                                    f.write('---->>>>RELOAD MODEL FROM EPOCH {}\n'.format(epoch_improve))
                                model.load_state_dict(torch.load(os.path.join(config.save_path, model_name)))
                                epoch_improve = ep
                                optimizer = create_optimizer(model, config, False, learning_rate=config.learning_rate * (
                                        1 - restart_used / (2 + config.max_restart)))

                                scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda2)
                                # scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=int(
                                #     config.warmup_steps * (total_steps - global_steps // lr_change_step) / total_steps),
                                #                                             num_training_steps=total_steps - global_steps // lr_change_step)

                    if test_results['f1_arg'] > f1_test:
                        f1_test = test_results['f1_arg']
                        print('-->Test new best score! f1_test = ', f1_test)
                        torch.save(model.state_dict(), os.path.join(config.save_path, model_test_name))
                        with open(os.path.join(config.save_path, log_name), 'a', encoding='utf-8') as f:
                            f.write('\t\t\t    Test Results: P= {:.4f} - R= {:.4f} - F= {:.4f} \n\
                                    Results argument: P= {:.4f} - R= {:.4f} - F= {:.4f} \n\t--=>>>Test New best score!\n'.format(
                                test_results['precision'],
                                test_results['recall'],
                                test_results['f1'],
                                test_results['precision_arg'],
                                test_results['recall_arg'],
                                test_results['f1_arg']))
                    else:
                        with open(os.path.join(config.save_path, log_name), 'a', encoding='utf-8') as f:
                            f.write('\t\t\t    Test Results: P= {:.4f} - R= {:.4f} - F= {:.4f}\n\
                                    Results argument: P= {:.4f} - R= {:.4f} - F= {:.4f} \n'.format(
                                test_results['precision'],
                                test_results['recall'],
                                test_results['f1'],
                                test_results['precision_arg'],
                                test_results['recall_arg'],
                                test_results['f1_arg']))

            if restart_used > config.max_restart:
                print('Restarting model is run out')
                break
        # if (ep + 1) % 5 == 0:
        #     output.clear()
    print('-->FINAL TEST')
    test_results = evaluate(config, test_dataset, model, tokenizer, prefix='test set - final test')

